#!/usr/bin/env python
##############################################################
#### This routine turns img*.fits.gz into a RAMSES movie. ####
####                                                      ####
####                                                      ####
##############################################################
import utils.CreateMovie as movie
import matplotlib as mpl
mpl.use('Agg')
import pythonstartup
import matplotlib.pyplot as plt
import numpy as np
import pyfits
import os
from matplotlib.colors import LogNorm
from matplotlib.colors import SymLogNorm
from matplotlib.colors import Normalize
import utils.get_labels
from argparse import ArgumentParser
import astropy.units as U
import astropy.constants as C
import copy
#####################################################################################################
# NB : to run on Curie, comment lines using SymLogNorm (conflict with older version of matplotlib
# and add this before importing plt (to avoid calling X) :
# import matplotlib as mpl
# mpl.use('Agg')
#####################################################################################################


#argument and option parser
parser = ArgumentParser()
parser.add_argument("-t", "--type", dest="typ", default='zvel',
                  help="type of variable : zvel (default), T, rho, ...", metavar="TYPE")
parser.add_argument("-l", "--show-labels", dest="show_labels", default=False, action='store_true',
                  help="show labels on the plot")
parser.add_argument("-b", "--show-bh", dest="show_bh", default=False, action='store_true',
                  help="show BH location on the plot")
parser.add_argument("--no-abs", dest="abs", default=True, action='store_false',
                  help="do not take -1*v_z for bottom half of the box")
parser.add_argument("--no-skip", dest="skip", default=True, action='store_false',
                  help="do not skip existing jpg files")
parser.add_argument("--no-movie", dest="no_movie", default=False, action='store_true',
                  help="generate jpg files but do not create movie")
parser.add_argument("--starts-with", dest="img", default='img', metavar='IMG',
                  help="first characters in the name of the files to read (default: img)")

args = parser.parse_args()

typ = args.typ
show_labels = args.show_labels
show_bh = args.show_bh
abs = args.abs
skip = args.skip
img = args.img

sfr=False
if typ == 'sfr':
    sfr=True
    files2=[]

files = []
dir = os.getcwd()+'/'
for file in os.listdir(dir):
    if not sfr and file.startswith(img) and file.endswith(typ+'.fits.gz'):
        files.append(file)
    if sfr and file.endswith('rho.fits.gz'):
        files.append(file)
        if sfr and file.endswith('Tedgetemp.fits.gz'): #SUPER IDEE MAIS STUPIDE EN FAIT !!!! parce que rho montre le disque de face et Tedgetemp par la tranche...
        files2.append(file)
files.sort() #files need to be sorted on Curie

if sfr:
    files2.sort()
    n = 1.5
    epsilon=0.01
    alpha = np.sqrt(32.*C.G/(3.*np.pi))
    Hcc = U.M_p/(U.cm)**3
    #print '1 H/cc = ', Hcc.to("Msun/pc^3"), " Msun/pc^3"

# Plot a given frame
def plotFunction(frame, typ):
    print 'Processing ', files[frame], ' : file ', frame+1, ' on ', len(files)
    #plt.figure()
    data_cube, header = pyfits.getdata(files[frame], header=True)
    if sfr:
        data_cube2, header2 = pyfits.getdata(files2[frame], header=True)

        rho_threshold = 10.
        T_threshold = 1e4
        rho_masked_with_T = copy.deepcopy(data_cube)
        rho_masked_with_T[(rho_masked_with_T < rho_threshold) & (data_cube2 > T_threshold)] = 0
        rho_masked_with_T = rho_masked_with_T*Hcc #handle unit
        rhoSFR = (epsilon*alpha*(rho_masked_with_T)**n).to("Msun/(yr.kpc^3)")
        rhoSFR_max = (0.3*9.9/U.Myr*rho_masked_with_T).to("Msun/(yr.kpc^3)")
        tt = rhoSFR_max-rhoSFR
        test = rhoSFR.value*0 + 1
        test[rhoSFR>rhoSFR_max] = 0
        #print test[test==0]
        test2 = rhoSFR_max.value*0 + 1
        test2[rhoSFR<=rhoSFR_max] = 0
        #print test2[test2==0]
        rhoSFR= copy.deepcopy(test*rhoSFR+test2*rhoSFR_max) #final rhoSFR
        data_cube = copy.deepcopy(rhoSFR.value)
        unit_sfr = rhoSFR.unit.to_string(format='latex')
    
    col_map = 'jet_r'
    if typ == 'v_z' or typ == 'v_y' or typ == 'v_x' or typ =='zvel':
        if (abs):
            my_norm = LogNorm(vmin=1, vmax=1e4)
            col_map = 'gray'
            
            #set lower-part of the box to opposite value
            npixels = len(data_cube[0,:])
            data_cube[0:npixels/2,:] *= -1.0
            #set inflowing values to "zero"
            data_cube[data_cube < 0] = 1e-10
            typ = 'v_out' #only shown on figure (does not change file name)
        
        else:
            my_norm = SymLogNorm(1, vmin=-1e4, vmax=1e4)
            col_map = 'RdBu'
    elif typ == 'T' or typ =='Tedgetemp':
        my_norm = LogNorm(vmin=1e4, vmax=10**7.5)
        col_map = 'gist_heat'
    elif typ == 'rho':
        my_norm = LogNorm(vmin=1e-7, vmax=1e6)
    elif typ == 'level':
        my_norm = Normalize(vmin=9, vmax=16)
    elif typ == 'partmass':
        my_norm = LogNorm(vmin=1e5, vmax=1e9)
    elif typ == 'sfr':
        my_norm = LogNorm(vmin=1e-5, vmax=1e11)
    #add new types here :
    #elif typ == 'new':
    #    my_norm = Normalize(vmin=new_vmin, vmax=new_vmax)
    else:
        print 'Unknown type !'
        my_norm = LogNorm()
        
    #get header parameters
    boxlen = header['boxlen']
    xlab, ylab = utils.get_labels(header)
    xmin, xmax = header['xmin'], header['xmax']
    ymin, ymax = header['ymin'], header['ymax']
    time = header['time'] #Myrs
    unit = header['*unit*']
    unit = (str(unit).split("'"))[1]
    unit = unit.strip()
    if sfr:
        unit = unit_sfr
    if len(unit) > 0:
        unit = ' ['+unit+']'
    img = plt.imshow(data_cube, norm=my_norm, cmap=col_map, aspect='equal', extent=[xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen], origin='lower')

    if typ == 'T' or typ == 'Tedgetemp':
        img.cmap.set_under(color='black')

    if (show_bh):
        xsink, ysink = header['xsink'], header['ysink']
        xsink, ysink = header['xsink'], header['ysink']
        plt.plot(xsink, ysink, 'w+', ms=20, mew=5)
        plt.plot(xsink, ysink, 'b+', ms=15, mew=3)
        plt.axis([xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen])

    cb = plt.colorbar(orientation='vertical')
    if typ == 'level':
        cb.set_ticks([9,10,11,12,13,14,15,16])
    if not abs and (typ == 'v_x' or typ == 'v_y' or typ == 'v_z' or typ == 'zvel'):
        cb.set_ticks([-1e4,-1e3,-1e2,-1e1,-1e0,0,1e0,1e1,1e2,1e3,1e4])
    ctitle = typ + unit
    cb.set_label(ctitle)

    plt.title('t = '+str("%0.2f" % time)+' Myrs')
    if (show_labels):
        plt.xlabel(xlab+' ['+(str(header['*xsink*']).split())[-2]+']')
        plt.ylabel(ylab+' ['+(str(header['*ysink*']).split())[-2]+']') #supposed to be 'kpc'

    else:
        ax = plt.gca()
        ax.set_xticks([])
        ax.set_yticks([])
        yline = ymin*boxlen + 0.05*(ymax - ymin)*boxlen
        xline_1 = xmin*boxlen + 0.1*(xmax - xmin)*boxlen
        scale_bar = 10 #kpc
        plt.hlines(yline, xline_1, xline_1 + scale_bar, label=str(scale_bar)+' kpc', colors='white', linewidth=3)
        plt.text(np.mean([xline_1, xline_1 + scale_bar]), yline + 0.02*yline,  str(scale_bar)+' kpc', \
            color='white', horizontalalignment='center', verticalalignment='center', \
            size='medium', weight='bold')
    #plt.show()

#                               numberOfFrames
movie.CreateMovie(plotFunction, len(files), typ, skip_files=skip, no_movie=args.no_movie)


