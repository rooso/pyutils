#!/usr/bin/env python
'''
Create SFR map from rho and T maps.
'''
import pythonstartup
from mpl_toolkits.axes_grid1 import make_axes_locatable
import sys, os
from astropy.io import fits as pyfits
import utils.get_labels
import utils.get_dir_name
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import astropy.units as U
import astropy.constants as C
import copy

from argparse import ArgumentParser

#argument and option parser
parser = ArgumentParser()
parser.add_argument("outpt", type=str, #nargs='+'
                  help="output number", metavar="OUTPUT")
parser.add_argument("--rho-threshold", dest="rho_threshold", default=10., type=float,
                  help="threshold value for density")
parser.add_argument("--T-threshold", dest="T_threshold", default=1e4, type=float,
                  help="threshold value for density")
parser.add_argument("--disk", dest="disk", default=False, action='store_true',
                  help="disk view")
parser.add_argument("--large", dest="large", default=False, action='store_true',
                  help="large scale view")
parser.add_argument("--silent", dest="silent", default=False, action='store_true',
                  help="do not print messages")

args = parser.parse_args()
outpt = args.outpt
rho_threshold = args.rho_threshold
T_threshold = args.T_threshold
silent = args.silent

if args.disk and not args.large:
    disk = '_disk'
elif args.large and not args.disk:
    disk = '_large'
elif args.large and args.disk:
    disk = '_disk_large'
else:
    disk=''

figname = 'sfr_map_'+outpt+disk
if T_threshold != parser.get_default('T_threshold'):
    figname += '_%.1eK' % T_threshold
if rho_threshold != parser.get_default('rho_threshold'):
    figname += '_%.1eH/cc' % rho_threshold

#outpt = '00200'
#rho_threshold = 10. #H/cc
#T_threshold = 2e4 #K

fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(13, 5), sharex=True, sharey=True)
#fig = plt.figure(figsize=(12, 6))
fig.subplots_adjust(wspace=0.3, hspace=0.3)

if os.path.isfile('map_'+outpt+'_rho'+disk+'.fits.gz') and os.path.isfile('map_'+outpt+'_T'+disk+'.fits.gz'): #check existence of files first
            
    if not silent:
        print 'map_'+outpt+'_rho'+disk+'.fits.gz', 'map_'+outpt+'_T'+disk+'.fits.gz'
    data_cube_rho, header_rho = pyfits.getdata('map_'+outpt+'_rho'+disk+'.fits.gz', header=True)
    data_cube_T,   header_T = pyfits.getdata('map_'+outpt+'_T'+disk+'.fits.gz', header=True)
    
    #get header parameters
    boxlen = header_rho['boxlen']
    if boxlen != header_T['boxlen']:
        print 'Boxlen error'
        sys.exit(2)
    lmax = header_rho['lmax']
    if lmax != header_T['lmax']:
        print 'lmax error'
        sys.exit(2)
    xlab, ylab = utils.get_labels(header_rho)
    xlab2, ylab2 = utils.get_labels(header_T)
    if xlab != xlab2 or ylab != ylab2:
        print xlab, ylab, xlab2, ylab2
        print 'View error'
        sys.exit(2)
    xmin, xmax = header_rho['xmin'], header_rho['xmax']
    ymin, ymax = header_rho['ymin'], header_rho['ymax']
    if xmin != header_T['xmin'] or ymin != header_T['ymin'] or xmax != header_T['xmax'] or ymax != header_T['ymax']:
        print 'Limit error'
        sys.exit(2)
    time = header_rho['time'] #Myrs
    if time != header_T['time']:
        print 'Time error'
        sys.exit(2)
    unit = utils.get_unit(header_rho)
    unit_T = utils.get_unit(header_T)

    im1=ax1.imshow(data_cube_rho, norm=LogNorm(vmin=1e-7, vmax=1e7), cmap='jet_r', aspect='equal', \
                            extent=[xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen], origin='lower')
    divider1 = make_axes_locatable(ax1)
    cax1 = divider1.append_axes("right", size="10%", pad=0.05)
    cb1 = plt.colorbar(im1, cax=cax1)
    cb1.set_label('rho'+unit)
    
    im2=ax2.imshow(data_cube_T, norm=LogNorm(vmin=1e2, vmax=1e10), cmap='jet', aspect='equal', \
                            extent=[xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen], origin='lower')
    divider2 = make_axes_locatable(ax2)
    cax2 = divider2.append_axes("right", size="10%", pad=0.05)
    cb2 = plt.colorbar(im2, cax=cax2)
    cb2.set_label('T'+unit_T)

    if not silent:
        print 'Min/max log rho ', np.min(np.log10(data_cube_rho)), np.max(np.log10(data_cube_rho))
        print 'Min/max log T   ', np.min(np.log10(data_cube_T)), np.max(np.log10(data_cube_T))

    rho_masked_with_T = copy.deepcopy(data_cube_rho)
    rho_masked_with_T[(rho_masked_with_T < rho_threshold) & (data_cube_T > T_threshold)] = 0

    n = 1.5
    epsilon=0.01
    alpha = np.sqrt(32.*C.G/(3.*np.pi))

    Hcc = U.M_p/(U.cm)**3
    #print '1 H/cc = ', Hcc.to("Msun/pc^3"), " Msun/pc^3" 
    rho_masked_with_T = rho_masked_with_T*Hcc

    rhoSFR = (epsilon*alpha*(rho_masked_with_T)**n).to("Msun/(yr.kpc^3)")
    rhoSFR_max = (0.3*9.9/U.Myr*rho_masked_with_T).to("Msun/(yr.kpc^3)")
    #print rhoSFR.shape, rhoSFR_max.shape

    #print 'rhoSFR'
    #print np.min(rhoSFR), np.max(rhoSFR)
    #print 'Max' 
    #print np.min(rhoSFR_max), np.max(rhoSFR_max)
    #print 'Diff'
    #print np.min(rhoSFR_max-rhoSFR), np.max(rhoSFR_max-rhoSFR)
    tt = rhoSFR_max-rhoSFR
    if not silent:
        print 'Ici, rhoSFR>rhoSFR_max de :', -1*tt[tt<0]

    test = rhoSFR.value*0 + 1
    test[rhoSFR>rhoSFR_max] = 0
    #print test[test==0]
    test2 = rhoSFR_max.value*0 + 1
    test2[rhoSFR<=rhoSFR_max] = 0
    #print test2[test2==0]
    rhoSFR = copy.deepcopy(test*rhoSFR+test2*rhoSFR_max)
    if not silent:
        print 'Sur les {} pixels, {} sont <= rhoSFR_max et {} sont > rhoSFR_max.'.format(rhoSFR.size,len(test2[test2==0]),len(test[test==0]))
        print 'Min/max log rhoSFR>0   ', np.min(np.log10(rhoSFR.value[rhoSFR.value>0])), np.max(np.log10(rhoSFR.value[rhoSFR.value>0]))

    im3=ax3.imshow(rhoSFR.value, norm=LogNorm(vmin=1e-7, vmax=1e7), cmap='jet_r', aspect='equal', extent=[xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen], origin='lower')
    divider3 = make_axes_locatable(ax3)
    cax3 = divider3.append_axes("right", size="10%", pad=0.05)
    cb3 = plt.colorbar(im3, cax=cax3)
    cb3.set_label('rhoSFR [{}]'.format(rhoSFR.unit.to_string(format='latex')))

    xsink, ysink, zsink = header_rho['xsink'], header_rho['ysink'], header_rho['zsink']
    if abs(xsink-boxlen/2.) > 5 or abs(ysink-boxlen/2.) > 5 or abs(ysink-boxlen/2.) > 5: 
        print 'BH abnormally far from center !!'
        print 'xsink = {} kpc, ysink = {} kpc, zsink = {} kpc'.format(xsink, ysink, zsink)
    show_bh = True
    if (show_bh):
        for ax in [ax1,ax2,ax3]:
            ax.plot(xsink, ysink, 'w+', ms=20, mew=5)
            ax.plot(xsink, ysink, 'b+', ms=15, mew=3)
            ax.axis([xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen])

    plt.suptitle(utils.get_dir_name()+' : Density'+unit+', Temperature'+unit_T+', Volume density of SFR [{}]'.format(rhoSFR.unit.to_string(format='latex'))+', t='+str(time)+' Myrs.')
    #plt.tight_layout()

    interactive=False
    if interactive:
        import mpld3
        mpld3.show(fig)
    else:
        plt.show()
        plt.savefig(figname+'.eps')

else:

    print 'File map_'+outpt+'_rho'+disk+'.fits.gz or map_'+outpt+'_T'+disk+'.fits.gz not found.'
