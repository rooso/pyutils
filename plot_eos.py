#!/usr/bin/env python

'''
     This code was written by Florent Renaud
'''

import numpy as N
import pylab as P

import re
import fargs
import pythonstartup

def getlist(value, typ=float): #read value of namelist keyword
	result = []
	for v in value.split(','):
		tab = v.split('*')     #account for short notation : 5x50 = 50, 50, 50, 50, 50
		if len(tab) == 1:
			result.append(tab[0])
		else:
			for i in range(int(tab[0])):
				result.append(tab[1])

    return N.array([typ(float(tmp.upper().replace('D','E'))) for tmp in result]) #1.0D0 -> 1.0E0


##########################################################################################

argparser = fargs.Parser(name='EOS', description='', author='', email='florent.renaud@cea.fr', version='2.0', date='14 June 2011')
argparser.add('-opt', default='')
argparser.add('_extra')
arg = argparser.parse()

##########################################################################################

levelmin, levelmax, level_jeans, boxlen, mass_sph, m_refine, jeans_refine = 1,1,1,1.0,1.0,[1],[1]

if(arg._extra == []):
    arg._extra = ['namelist_0.nml']

for nml in arg._extra:
	
    f = open(nml, 'r')  #find following keywords in namelist

	for line in f:
		if re.match('levelmin', line) != None:
			levelmin = int(line.split('=')[1])
					
		elif re.match('levelmax', line) != None:
			levelmax = int(line.split('=')[1])
			
		elif re.match('level_jeans', line) != None:
			level_jeans = int(line.split('=')[1])
			if level_jeans == 0:
				level_jeans = levelmax
			
		elif re.match('boxlen', line) != None:
			boxlen = float(line.split('=')[1].upper().replace('D','E')) # in kpc
			
		elif re.match('mass_sph', line) != None:
			mass_sph = getlist(line.split('=')[1]) * 1.0E9 # in Msun
			
		elif re.match('m_refine', line) != None:
			m_refine = getlist(line.split('=')[1], int)
	
		elif re.match('T2_star', line) != None:
			T2_star = float(line.split('=')[1].upper().replace('D','E')) # in K
	
		elif re.match('g_star', line) != None: #gravitational constant in correct units
			gamma = float(line.split('=')[1].upper().replace('D','E'))
	
		elif re.match('eos_type', line) != None:
			eos_type = (line.split('=')[1])[1:-2]
	### not used yet
	##	elif re.match('jeans_refine', line) != None:
	##		jeans_refine = getlist(line.split('=')[1], int)
	#
	f.close()
	
	spherical_collapse = False
	
	#Low density parameters (from fit of actual heating/cooling measures at low metalicity)
	rho_low_density = 1.0E-3 # in H/cc
	T_low_density = 4.0E6 # in K
	
	#Pseudo-cooling parameters (from fit of actual heating/cooling measures)
	rho_pseudo_cooling = 10.0**(-0.5) # in H/cc
	gamma_pc = 1./2. # gamma
	
	#Self-shielding parameters
	rho_ss1 = 0.1 # in H/cc
	rho_ss2 = 10 # in H/cc
	T_ss1 = 100.0 # in K
	
	rho = N.logspace(N.log10(1.0E-4), N.log10(1.0E7), 10000) # min [H/cc], max [H/cc], nb point	
	T = N.zeros_like(rho)
	g = N.zeros_like(rho)
	dxmax = N.zeros_like(rho)
	dxmax_opt = N.zeros_like(rho)
	xtext = N.zeros(levelmax-levelmin)
	ytext = N.zeros(levelmax-levelmin)
	
	if len(m_refine) == 1:
		m_refine = N.ones(levelmax-levelmin) * m_refine[0]
	if len(mass_sph) == 1:
		mass_sph = N.ones(levelmax-levelmin) * mass_sph[0]
	
	# Free-fall time factor
	if spherical_collapse:
		t_ff_factor = N.sqrt(3./32.) # spherical collapse
	else:
		t_ff_factor = 1.0 # infinite 1D sinusoidal collapse
	
	# Low densities
	mask = (rho<rho_low_density)
	g[mask] = gamma
	T[mask] = T_low_density * (rho[mask]/rho_low_density)**(gamma - 1.)
	
	if eos_type=='pseudo_cooling':
		# medium densities: isothermal
		mask = (rho>=rho_low_density)*(rho<rho_pseudo_cooling)
		g[mask] = 1.0
		T[mask] = T2_star
		# high densities:
		mask = (rho>=rho_pseudo_cooling)
		g[mask] = gamma_pc
		T[mask] = T2_star * (rho[mask]/rho_pseudo_cooling)**(gamma_pc - 1.)
	elif eos_type=='self_shielding':
		# medium densities: isothermal
		mask = (rho>=rho_low_density)*(rho<rho_ss1)
		g[mask] = 1.0
		T[mask] = T2_star
		# medium densities2:
		mask = (rho>=rho_ss1)*(rho<rho_ss2)
		g[mask] = (N.log10(T2_star)-N.log10(T_ss1)) / N.log10(rho_ss1/rho_ss2) + 1.0
		T[mask] = T2_star * (rho[mask]/rho_ss1)**(g[mask] - 1.)
		# high densities:
		mask = (rho>=rho_ss2)
		g[mask] = 4./5.
		T[mask] = T2_star * (rho_ss2/rho_ss1)**( (N.log10(T2_star)-N.log10(T_ss1)) / N.log10(rho_ss1/rho_ss2) ) * (rho[mask]/rho_ss2)**(g[mask] - 1.)
	elif eos_type=='self_shielding_floor':
		# medium densities: isothermal
		mask = (rho>=rho_low_density)*(rho<rho_ss1)
		g[mask] = 1.0
		T[mask] = T2_star
		# medium densities2:
		mask = (rho>=rho_ss1)*(rho<rho_ss2)
		g[mask] = (N.log10(T2_star)-N.log10(T_ss1)) / N.log10(rho_ss1/rho_ss2) + 1.0
		T[mask] = T2_star * (rho[mask]/rho_ss1)**(g[mask] - 1.)
		# high densities: isothermal
		mask = (rho>=rho_ss2)
		g[mask] = 1.0
		T[mask] = T2_star * (rho_ss2/rho_ss1)**((N.log10(T2_star)-N.log10(T_ss1)) / N.log10(rho_ss1/rho_ss2))
	else:
		# isothermal
		mask = (rho>= rho_low_density)
		g[mask] = 1.0
		T[mask] = T2_star
	
	#pc    = 3.085677e16 # in m
	#mH    = 1.66E-27 # in kg
	#Msun  = 1.9889E30 # in kg
	#Myr   = 3.15576E13 # in s 
	#G     = 6.67428e-11 # in m**3 / kg / (s**2)
	#kB    = 1.3806504E-23 # in m**2 kg s**-2 K**-1  (or in J/K)
	#c     = 299792458.0 # in m / s
		
	# Jeans polytropic EOS
	#P_Jeans = 16 * l_jeans**2 * rho**2 * G / (gamma * pi)
	#T_Jeans = P_jeans / rho * mH / kB
	T_Jeans = 16.0 / t_ff_factor**2 * (boxlen/2**level_jeans)**2 * rho / gamma / N.pi * 1.2683463E4  # in K
	
	# effective EOS
	T_eos = N.max(N.array([T, T_Jeans]), axis=0)
	g[(T_eos == T_Jeans)] = 2.0 # warning here
		
	# free-fall time = t_ff_factor * (pi / (rho * G) )^(1/2)
	t_ff = t_ff_factor * N.sqrt(N.pi/rho) *  3.0043033E15 # in s
	
	# speed of sound = (gamma * T * kB / mH )^(1/2)
#	cs = N.sqrt(g * T_eos) * 9.1198528E1 # in m/s
	cs = N.sqrt(gamma * T_eos) * 9.1198528E1 # in m/s
	
	# Jeans lenght = cs * t_ff
	l_Jeans = cs * t_ff / 3.085677E16 # in pc
#	l_Jeans[N.isnan(l_Jeans)] = 0.0 # negative sound speed for gamma <= 0.0

	# Jeans mass = 4/3 pi * rho * (l_Jeans/2.0)^3
	
	if(arg.opt):
		# Truelove criterion
		mass_sph_opt = ''
		m_refine_opt = ''
		for l in range(levelmin, levelmax):
			dx_loc = boxlen / 2**l * 1000. # in pc
			rho_max = (mass_sph[l-levelmin] * m_refine[l-levelmin] / dx_loc**3) *  40.7806 # in H/cc
			
			# Optimized arrays
			rho_max_opt = rho[N.argmin((l_Jeans/4. - dx_loc)**2)]
			mass_sph_opt += str( rho_max_opt * dx_loc**3 / m_refine[l-levelmin] * 2.45215E-11 )+','  # in 10^9 Msun
			m_refine_opt += str( int(N.floor(rho_max_opt * dx_loc**3 / mass_sph[l-levelmin] * 2.45215E-2)) )+','
		
			if l == levelmin:
				dxmax[:] = dx_loc # in pc
				dxmax_opt[:] = dx_loc # in pc
			dxmax[(rho>rho_max)] = dx_loc / 2. # in pc
			dxmax_opt[(rho>rho_max_opt)] = dx_loc / 2. # in pc
			xtext[l-levelmin] = rho_max_opt
			ytext[l-levelmin] = dx_loc
		
		print 'Optimized mass_sph [in 1E9 Msun]: '+mass_sph_opt[0:-1]
		print 'Optimized m_refine              : '+m_refine_opt[0:-1]
	
	
	p1 = P.subplot(2,1,1)
	#P.subplots_adjust(hspace=0)
	
	P.loglog(rho, T_eos, label=nml)
	N.savetxt('rho.txt', rho)
	N.savetxt('T.txt', T_eos)
	
	if(len(arg._extra) > 1): P.legend()	
	
	#P.xlabel('rho [H/cc]')
	P.ylabel('T [K]')
	P.ylim(1,1e7)
	
	p2 = P.subplot(2,1,2)
	
	P.loglog(rho, l_Jeans/4., label='L_Jeans / 4')
	
	if(arg.opt):
		for i in range(levelmin,levelmax,3):
			P.text(xtext[i-levelmin], ytext[i-levelmin], i, ha='right')
		P.loglog(rho, dxmax, '--', label='L (namelist)')
		P.loglog(rho, dxmax_opt, ':', label='L (best)')
		P.legend()
	
	P.xlabel("rho [H/cc]")
	P.ylabel("l [pc]")

P.show()


