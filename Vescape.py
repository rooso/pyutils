#!/usr/bin/env python
'''
Reads output of V_esc.exe input.dat (make to re-compile V_esc.f90) and input.dat (which is actually the output of part2map -inp outpt -newstar -ascii renamed to input.dat)
NB: V_esce.f90 modified to read new input.dat format correctly (ignore vx,vy and vz columns and convert kpc to pc)
'''

#import matplotlib as mpl #uncomment to generate plots on curie
#mpl.use('Agg')
import pythonstartup
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import make_axes_locatable
import utils.find_colors
import utils.progressbar

print 'Reading data...'
                                  #0  1  2   3    4    5    6   7     8         9
data = np.genfromtxt('input.dat') #x, y, z, v_x, v_y, v_z, rho, T, cell_size, level
                                  #in kpc       km/s       H/cc K     kpc       -
data2 = np.genfromtxt('VESC.DAT') #id, v_esc
                                  #--  km/s

sub_vesc = 0
sup_vesc = 0
mass_sub_vesc = 0
mass_sup_vesc = 0

norm = []
#print np.shape(data)
#print np.shape(data2)

#sub-arrays
d2 = data2
d  = data
#d2 = d2[d[:,0]>90]
#d  = d [d[:,0]>90]
#d2 = d2[d[:,0]<110]
#d  = d [d[:,0]<110]
d2 = d2[d[:,1]>90]
d  = d [d[:,1]>90]
d2 = d2[d[:,1]<110]
d  = d [d[:,1]<110]

print 'Skipping points...'
skip=1 #skip points if necessary
partid = d2[::skip,0]
v_esc = d2[::skip,1]
x=d[::skip,0]
y=d[::skip,1]
z=d[::skip,2]
v_x=d[::skip,3]
v_y=d[::skip,4]
v_z=d[::skip,5]
mass=40*d[::skip,6]*(d[::skip,8]*1e3)**3 #1 Msun/pc3 = 40 H/cc 

print 'Initial number of points :', len(data[:,0])
print 'Current number of points :', len(x)
print float(len(x))/len(d[:,0])*100, ' % were kept.'

#print x, y, z

print 'Computing velocities...'
for idx,val in enumerate(partid):
    n=np.sqrt(v_x[idx]**2+v_y[idx]**2+v_z[idx]**2)
    norm.append(n)
    if n > v_esc[idx]:
        #print n, v_esc[idx]
        sup_vesc = sup_vesc + 1
        mass_sup_vesc = mass_sup_vesc + mass[idx]
    else:
        sub_vesc = sub_vesc + 1
        mass_sub_vesc = mass_sub_vesc + mass[idx]

    pb = utils.progressbar(idx,len(partid)-1)

print '\n'

print round(float(sup_vesc)/(len(partid)-1)*100,3), ' % of parts have velocity > vesc'
print round(float(sub_vesc)/(len(partid)-1)*100,3), ' % of parts have velocity < vesc'

print round(mass_sup_vesc/(sum(mass))*100,3), ' % of mass has velocity > vesc'
print round(mass_sub_vesc/(sum(mass))*100,3), ' % of mass has velocity < vesc'

print 'Now attempting to plot...'
fig = plt.figure()
ax = Axes3D(fig)
ax.set_xlim3d(0, 200)
ax.set_ylim3d(0, 200)
ax.set_zlim3d(0, 200)
size = 4*(np.round(np.log10(mass/sum(mass)))+abs(min(np.round(np.log10(mass/sum(mass)))))+1)
scat = ax.scatter(x, y, z, c=np.log10(norm/v_esc), cmap='RdBu', edgecolors='None', linewidth=0, s=size, vmin=-3, vmax=3, marker='.', alpha=0.6)
ax.set_xlabel('x [kpc]')
ax.set_ylabel('y [kpc]')
ax.set_zlabel('z [kpc]')
ax.view_init(20,-120)
#divider = make_axes_locatable(ax)
#cax = divider.append_axes("top", size="5%", pad=0.05)
ax2 = plt.gcf().add_axes([0.8, 0.3, 0.025, 0.4]) #left,bottom,width,height
cbar = fig.colorbar(scat,ax=ax, cax=ax2)#,fraction=0.046)#, pad=0.04)
cbar.set_label("log (v/v_esc)")
cbar.set_alpha(1)
cbar.draw_all()
fig.set_tight_layout(True)
plt.show()
plt.savefig('vesc.pdf')
