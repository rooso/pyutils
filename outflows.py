#!/usr/bin/env python
'''
######################################################################################################################################################
##### This program generates plot of outflow_rate vs time for following z_surfaces (from bottom of the box) or shells (radius from center of box) ####
##### from specified namelist                                                                                                                     ####
##### NB : galaxy is at ~ 100 kpc in z                                                                                                            ####
##### /!\ for z_surfaces : upward outflow and downward outflow are SUMMED (not averaged)                                                          ####
######################################################################################################################################################
'''
import pythonstartup
import os, sys
import numpy as np
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import utils.find_colors
import utils.get_dir_name
import astropy.units as U
from argparse import ArgumentParser

def mass_loading(time,data_sfr,mpm,sfr_name='fiducial'):
    import numpy as np
    print 'Max \t Mean outflow rate / '+sfr_name+' SFR (average over ', time[-1]-time[0], ')'
    #print np.shape(time), np.shape(data_sfr[:,0]), np.shape(data_sfr[:,1]) 
    sfr_interp = np.interp(time,data_sfr[:,0],data_sfr[:,1])
    #print np.shape(sfr_interp), np.shape(mpm[0,:]), np.shape(mpm[1,:])
    max_mass_loading=mpm[0,:]/sfr_interp
    mean_mass_loading = mpm[1,:]/sfr_interp
    #print np.shape(max_mass_loading), np.shape(mean_mass_loading)
    print "{0:.2f}".format(np.mean(max_mass_loading))+"\t {0:.2f}".format(np.mean(mean_mass_loading))

def per_moment(time,data,symbol,symcol,alpha=1):
    '''
    computes max and mean outflow rate among all shells for each time
    '''
    max_per_moment = []
    mean_per_moment = []
    for idx,val in enumerate(time):
        if proceeding:
            skip_col = 6 #/!\/!\/!\ NOT AUTOMATIC !!! #skip low altitude because small shells mostly in the disk
        else:
            skip_col = 1 #need at least 1 to skip 'TimeMyr' column
            
        t_max=[dt for dt in data.dtype.names[skip_col:]] 
        temp_max=[]
        for dt in t_max:
            temp_max.append(data[idx][dt]) #gather value of outflow rate for each surface at given moment
        max_per_moment.append(max(temp_max)) #create array of max for each time   print max_per_moment)
        
        t_mean=[dt for dt in data.dtype.names[max([skip_col,10]):15]] #ignore fluctuations from low altitude and late propagation at high altitude
        temp_mean=[]
        for dt in t_mean:
            temp_mean.append(data[idx][dt]) #gather value of outflow rate for each surface at given moment
        mean_per_moment.append(np.mean(temp_mean)) #create array of mean for each time   print max_per_moment)

    p_out = plt.plot(time,max_per_moment,color='black',label='Max', linewidth=2, marker=symbol,markerfacecolor=symcol,alpha=alpha)
    p_out = plt.plot(time,mean_per_moment,color='grey',label='Mean', marker=symbol,markerfacecolor=symcol,alpha=alpha)
    return np.array([max_per_moment,mean_per_moment])

#argument and option parser
parser = ArgumentParser()
###parser.add_argument("--correction", dest="correction", default=False, action='store_true',
###                  help="Correct outflow rate for space dilution")
parser.add_argument("--silent", dest="silent", default=False, action='store_true',
                  help="do not print messages")
parser.add_argument("--proceeding", dest="proceeding", default=False, action='store_true',
                  help="Proceeding plot")
parser.add_argument("--all", dest="all", default=False, action='store_true',
                  help="Plot KThR+AGN, KThR and AGN curves")
parser.add_argument("--lores", dest="lores", default=False, action='store_true',
                  help="For L1/2 and LL3, overplot LL1/2 and LLL3")
parser.add_argument("--max", dest="do_max", default=False, action='store_true',
                  help="Keep maximal outflow rate among all radii")
parser.add_argument("--mix", dest="mix", default=False, action='store_true',
                  help="For L1_KThR, L1_AGN, L2_KThR, L2_AGN ONLY : compute outflow for L1 or L2_KThR+AGN divided by current SFR for error bars")
parser.add_argument("--sfr", dest="num", default='',
                  help="Output number for SFR", metavar="NUM")
parser.add_argument("--sfr_lores", dest="num_lores", default='',
                  help="Output number for SFR (lores)", metavar="NUM")
args = parser.parse_args()
silent = args.silent
num = args.num
num_lores = args.num_lores
#all = args.all #all == Python keyword
do_max = args.do_max
proceeding = args.proceeding
lores = args.lores

#Handle size of plot
fig = plt.figure(figsize=(10, 6.5))

if proceeding:
    #fig = plt.figure(figsize=(15,10))
    fig = plt.figure(figsize=(8,6))

# Print current working directory
if not silent:
    print "Current working dir : %s" % os.getcwd()

dir = os.getcwd()+'/'
fig_title = utils.get_dir_name()

#Get figure name
figname = 'outflow_rate_'+fig_title
###if (not args.correction):
###    figname += '_no-correction'
    
sfr_file = 'part_00'+num+'.dat.sfr'
sfr_file_lores = 'part_00'+num_lores+'.dat.sfr'

if args.all:
    figname = 'outflow_rate_'+fig_title+'_all'

if do_max:
    figname = figname+'_max'

if lores:
    figname = figname+'_lores'
    transp = 0.3

#Extract data
data = np.genfromtxt(dir+'/outflow_vs_time_dat.txt', names=True, dtype='float')

if lores:
    dir_lores = dir.replace('O/L','O/LL')
    data_lores = np.genfromtxt(dir_lores+'/outflow_vs_time_dat.txt', names=True, dtype='float')

if args.all:
    if '/LL3' not in os.getcwd():
        data_KThR = np.genfromtxt(os.getcwd().replace('_KThR+AGN','')+'_KThR/outflow_vs_time_dat.txt', names=True, dtype='float')
        data_AGN = np.genfromtxt(os.getcwd().replace('_KThR+AGN','')+'_AGN/outflow_vs_time_dat.txt', names=True, dtype='float')
    if lores:
       data_KThR_lores = np.genfromtxt(dir_lores[:-1].replace('_KThR+AGN','')+'_KThR/outflow_vs_time_dat.txt', names=True, dtype='float')
       data_AGN_lores = np.genfromtxt(dir_lores[:-1].replace('_KThR+AGN','')+'_AGN/outflow_vs_time_dat.txt', names=True, dtype='float') 
    
if args.mix: #for L1_KThR, L1_AGN, L2_KThR, L2_AGN, LLL3_KThR and LLL3_AGN, divide KThR+AGN outflow by current SFR to get all error bars on mass loadings
    if '/L1' in os.getcwd():
        dirname = '/Users/oroos/Post-stage/SIMUS/LOGO/L1'
    elif '/L2' in os.getcwd():
        dirname = '/Users/oroos/Post-stage/SIMUS/LOGO/L2_KThR+AGN'
    elif '/LLL3' in os.getcwd():
        dirname = '/Users/oroos/Post-stage/SIMUS/LOGO/LLL3'
    else:
        print 'Mix not possible'
        sys.exit(0)
    if '/L1/' in os.getcwd() or '/L2_KThR+AGN/' in os.getcwd() or '/LLL3/' in os.getcwd():
        print 'Mix impossible, only AGN or KThR'
        sys.exit(0)
    
    data_KThR_AGN = np.genfromtxt(dirname+'/outflow_vs_time_dat.txt', names=True, dtype='float')
    if lores:
        data_KThR_AGN_lores = np.genfromtxt(dirname.replace('O/L','O/LL')+'/outflow_vs_time_dat.txt', names=True, dtype='float')


#print data.dtype.names
time = data["timeMyr"]*U.Myr
if lores:
    time_lores = data_lores["timeMyr"]*U.Myr
if args.all:
    if '/LL3' not in os.getcwd():
        time_KThR = data_KThR["timeMyr"]*U.Myr
        time_AGN = data_AGN["timeMyr"]*U.Myr
    if lores:
        time_KThR_lores = data_KThR_lores["timeMyr"]*U.Myr
        time_AGN_lores = data_AGN_lores["timeMyr"]*U.Myr
if args.mix:
    time_KThR_AGN = data_KThR_AGN["timeMyr"]*U.Myr
    if lores:
        time_KThR_AGN_lores = data_KThR_AGN_lores["timeMyr"]*U.Myr

z_surf = []
for val in range(len(data.dtype.names)):
    if val != 0:
        z_surf.append(float(data.dtype.names[val]))
#print z_surf

z_surf = z_surf*U.kpc
nsurf = len(z_surf)

#Plot data
handles = []
labels = []
for val in z_surf:
    labels.append(str(val))
#print labels

colorVal = utils.find_colors(z_surf.value, [min(z_surf.value), max(z_surf.value)], cmap='jet_r')

max_rate = []
min_rate = []

for idx, val in enumerate(data.dtype.names): #idx is the index of the surface
    if val != "timeMyr": 
        colorText = labels[idx-1]
        #print data[val]
        #print val

        if proceeding and colorText in ['6.0 kpc', '8.0 kpc', '10.0 kpc', '12.0 kpc', '14.0 kpc', '16.0 kpc']:#, '55.0 kpc', '60.0 kpc', '70.0 kpc', '80.0 kpc', '90.0 kpc', '100.0 kpc']:
            #do nothing
            a=None
        else:
            if not (do_max and args.all):
                p_out=plt.plot(time,data[val],marker='o', color=colorVal[idx-1], label=colorText, linewidth=2)
                #print time, data[val]
                if lores:
                    p_out=plt.plot(time_lores,data_lores[val],marker='o', color=colorVal[idx-1], label=colorText, linewidth=2, alpha=transp)
                handles.append(mlines.Line2D([], [], label=colorText, linewidth=4, color=colorVal[idx-1]))
                ###blue_line = mlines.Line2D([], [], color='blue', marker='*', markersize=10, label='Blue stars', linewidth=2) #customized legend
            if args.all:
                if not do_max:
                    if '/LL3' not in os.getcwd():
                        p_out=plt.plot(time_KThR,data_KThR[val],marker='*', color=colorVal[idx-1], label=colorText, linewidth=1, linestyle='-')
                        p_out=plt.plot(time_AGN,data_AGN[val],marker='s', color=colorVal[idx-1], label=colorText, linewidth=2, linestyle='--')
                    if lores:
                         p_out=plt.plot(time_KThR_lores,data_KThR_lores[val],marker='*', color=colorVal[idx-1], label=colorText, linewidth=1, linestyle='-', alpha=transp)
                         p_out=plt.plot(time_AGN_lores,data_AGN_lores[val],marker='s', color=colorVal[idx-1], label=colorText, linewidth=2, linestyle='--', alpha=transp)
        
        min_rate.append(min(data[val])) #those min and max are wrt each shell.
        max_rate.append(max(data[val]))

        #print min(data[val]), max(data[val])
        #if max(data[val]) == 0:
        #    if not silent:
        #        print "No outflow through", colorText, " surface."

if do_max:
    troll=None
    if args.all:
        troll='green'
    mpm=per_moment(time,data,'o',troll)
    if lores:
        mpm_lores=per_moment(time_lores,data_lores,'o',troll, alpha=transp)
    handles.append(mlines.Line2D([], [], label='Mean', linewidth=4, color='grey'))
    handles.append(mlines.Line2D([], [], label='Max', linewidth=4, color='black'))
    if args.all:
        if '/LL3' not in os.getcwd():
            mpm_KThR=per_moment(time_KThR,data_KThR,'*','blue')
            mpm_AGN=per_moment(time_AGN,data_AGN,'s','red')
        if lores:
             mpm_KThR_lores=per_moment(time_KThR_lores,data_KThR_lores,'*','blue', alpha=transp)
             mpm_AGN_lores=per_moment(time_AGN_lores,data_AGN_lores,'s','red', alpha=transp)
    if args.mix:
        mpm_KThR_AGN=per_moment(time_KThR_AGN,data_KThR_AGN,'o','green')
        if lores:
            mpm_KThR_AGN_lores=per_moment(time_KThR_AGN_lores,data_KThR_AGN_lores,'o','green', alpha=transp)


handles=handles[::-1]

if os.path.isfile(dir+sfr_file): #check existence of file first
     data_sfr = np.genfromtxt(dir+sfr_file, dtype='float')
     if lores:
         data_sfr_lores = np.genfromtxt(dir_lores+sfr_file_lores, dtype='float')
     colsfr='black'
     if args.all or do_max:
         colsfr='brown'
     p_sfr=plt.plot(data_sfr[:,0],data_sfr[:,1],marker='^', color=colsfr, label='SFR', linewidth=2)
     if lores:
         p_sfr=plt.plot(data_sfr_lores[:,0],data_sfr_lores[:,1],marker='^', color=colsfr, label='SFR', linewidth=2,alpha=transp)
     handles.append(mlines.Line2D([], [], label='SFR', linewidth=4, color=colsfr))
     if do_max:
         print fig_title,
         ml=mass_loading(time,data_sfr,mpm,sfr_name=fig_title)
         if lores:
            print fig_title+'(lores)',
            ml_lores=mass_loading(time_lores,data_sfr_lores,mpm_lores,sfr_name=fig_title+'_lores')
         if args.all:
             if '/LL3' not in os.getcwd():
                 print fig_title+'(KThR only)',
                 ml_KThR=mass_loading(time_KThR,data_sfr,mpm_KThR,sfr_name=fig_title)
                 print fig_title+'(AGN only)',
                 ml_AGN=mass_loading(time_AGN,data_sfr,mpm_AGN,sfr_name=fig_title)
             if lores:
                 print fig_title+'(KThR only+lores)',
                 ml_KThR_lores=mass_loading(time_KThR_lores,data_sfr_lores,mpm_KThR_lores,sfr_name=fig_title+'_lores')
                 print fig_title+'(AGN only+lores)',
                 ml_AGN_lores=mass_loading(time_AGN_lores,data_sfr_lores,mpm_AGN_lores,sfr_name=fig_title+'_lores')
         if args.mix:
            print fig_title+'(KThR+AGN !)',
            ml_KThR_AGN = mass_loading(time_KThR_AGN,data_sfr,mpm_KThR_AGN,sfr_name=fig_title) #current SFR ! -> for error bars.
            if lores:
                print fig_title+'(KThR+AGN !+lores)',
                ml_KThR_AGN_lores = mass_loading(time_KThR_AGN_lores,data_sfr_lores,mpm_KThR_AGN_lores,sfr_name=fig_title+'_lores') #current loresSFR ! -> for error bars.

if args.all or ('/LL3' in os.getcwd() and lores):
    handles.append(mlines.Line2D([], [], label='AGN', linewidth=0, color='black', marker='s', markerfacecolor='red'))
    handles.append(mlines.Line2D([], [], label='Stellar', linewidth=0, color='black', marker='*', markerfacecolor='blue')) #label='KThR'
    handles.append(mlines.Line2D([], [], label='Stellar+AGN', linewidth=0, color='black', marker='o', markerfacecolor='green')) #label='KThR+AGN'   

plt.xlabel('Time in Myr')
plt.ylabel("Outflow rate in Msun/yr")
plt.xlim(min(time.value) - (max(time.value) - min(time.value))*0.02, max(time.value) + (max(time.value) - min(time.value))*0.4)
#plt.xlim(110, 160)
if proceeding:
    plt.ylim(10**(0),10**2.8)
#plt.ylim(min(min_rate)-0.5,max(max_rate)+3)
plt.yscale('log')
plt.title(fig_title)
plt.legend(handles=handles, fontsize=11)
fig.set_tight_layout(True)
plt.show()
plt.savefig(figname+'.pdf')


if not silent and not args.all and not do_max:
    print 'Surface in kpc \t Min outflow rate \t Max outflow rate in Msun/yr'
    for idx, val in enumerate(data.dtype.names): #idx is the index of the surface
        if val != "timeMyr":
            if max_rate[idx-1] != 0:
                print val, '\t\t\t', '{:.2e}'.format(min_rate[idx-1]), '\t\t\t', '{:.2e}'.format(max_rate[idx-1])
            else:
                print "No outflow through", val, " kpc surface."

#amr2map -inp output_00628/ -xmin 0.475 -ymin 0.475 -xmax 0.525 -ymax 0.525 -typ 4 -dir x -lmax 14 -zmin 0.3 -zmax 0.7
#amr2map -inp output_00628/ -xmin 0.475 -ymin 0.475 -xmax 0.525 -ymax 0.525 -typ 1 -dir x -lmax 14 -zmin 0.3 -zmax 0.7
#amr2map -inp output_00628/ -xmin 0.475 -ymin 0.475 -xmax 0.525 -ymax 0.525 -typ 7 -dir x -lmax 14 -zmin 0.3 -zmax 0.7
