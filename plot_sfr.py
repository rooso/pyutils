#!/usr/bin/env python

'''
    This code was writtent by Florent Renaud
'''

import pythonstartup
import numpy as N
import matplotlib.pyplot as plt
from astropy.io import fits as pyfits
import utils.get_dir_name
import os.path
import fargs

##########################################################################################

argparser = fargs.Parser(name='plot_sfr', description='Read a part file and plot an histogram of the time of birth of the stars', author='Florent Renaud', email='florent.renaud@cea.fr', version='1.0', date='30 November 2011')
argparser.add('_extra')
argparser.add('-bin', default=1.0, typ=float, help='Time bin for the histogram (in Myr)')
argparser.add('-now', default=0.0, typ=float, help='Time now (in Myr)')
argparser.add('-eta_sn', default=0.0, typ=float, help='Mass fraction of the SNe in the IMF. Used to correct for the mass loss of the stars. (Usual value = 0.2, set 0 for no correction)')
argparser.add('-f_w', default=0.0, typ=float, help='Loading factor of the feedback. Indicates when the correction of the stellar mass should be done. Thermal FB = 0; Kinetic FB > 0')
argparser.add('-out', help='Output as ascii')
arg = argparser.parse()

#Handle size of plot
fig = plt.figure(figsize=(12, 6))

##########################################################################################

for inp in arg._extra:

	if arg.now == 0:
		import utils.get_sim_time
		t=utils.get_sim_time(inp[5:10], option=2, string=False)
		if t[1]:
			print t[0]
			arg.now = t[0]
	
	f = N.loadtxt(inp)
	mask = (f[:,7]>=0) # get rid of strange ids
	#print arg.now
	#print 'map_'+inp[5:10]+'_T.fits.gz'
	if arg.now == 0 and os.path.isfile('map_'+inp[5:10]+'_T.fits.gz') :
		data_cube, header = pyfits.getdata('map_'+inp[5:10]+'_T.fits.gz', header=True)
		#get header parameters
		arg.now = header['time'] #Myr

	age = f[mask,7]
	tob = arg.now - age
	masses = f[mask,6]

	if(arg.eta_sn > 0.0): # SN feedback is injected. The mass of the stars is not the one at the time of there birth. Correction needed
		if(arg.f_w == 0.0): # thermal feedback: the mass of the SNe is reduced at the time of the blast. Correction must be made for the stars older than 10 Myr only.
			sn = (age > 10.0)
		else: # kinetic feedback: GMC particles have been created at the time of birth and carry eta_sn of the mass. Correction must be made for all the stars.
			sn = (age > 0.0)
		masses[sn] = masses[sn] / (1.-arg.eta_sn)  # mass_post_feedback = mass_at_birth - mass_at_birth * eta_sn  <==> mass_at_birth = mass_post_feedback / (1.-eta_sn)

	h, x = N.histogram(tob, bins=N.rint(( N.amax(tob)-N.amin(tob) ) / arg.bin), weights=masses)
	h = h/(arg.bin*1e6)

	if(arg.out):
		N.savetxt(inp+'.sfr', N.column_stack((x[:-1],h)))
	#else:
	print "Current SFR=", h[-1]," Msun / yr"
	plt.plot(x[:-1], h, label=inp)


plt.xlabel("time [Myr]")
plt.ylabel("SFR [Msun / yr]")
plt.legend(loc='best')
plt.show()
figname = 'sfr_'+utils.get_dir_name() #'sfr_'+inp[5:10] #
plt.suptitle(utils.get_dir_name())
plt.savefig(figname+'.eps')
