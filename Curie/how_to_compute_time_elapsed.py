#!/usr/bin/env python
#1. from Ramses logfile : cat logfile | grep 'Time elapsed' > time_elapsed

#2. in Python :
import numpy as np
import matplotlib.pyplot as plt

def coarse_dt():
    data = np.genfromtxt('time_elapsed',delimiter=':')

    #Time elapsed during run in h :
    time_elapsed = sum(data[:,1])/3600

    #Mean duration of coarse time step in s :
    mean_coarse_dt = np.mean(data[2:,1])
    mean_coarse_dt_less_1LB = np.mean(data[3:,1])

    strct = {"data":data,"time_elapsed":time_elapsed,"mean_coarse_dt":mean_coarse_dt, "mean_coarse_dt_less_1LB":mean_coarse_dt_less_1LB}

    return strct

if __name__ == "__main__":

    strct = coarse_dt()
    #Time elapsed during run in h :
    print 'Time elapsed during run in h :', strct["time_elapsed"]

    #Mean duration of coarse time step in s :
    print 'Mean duration of coarse time step in s :', strct["mean_coarse_dt"]
    print 'Mean duration of coarse time step in s : (excluding first LB)', strct["mean_coarse_dt_less_1LB"]

    #Plot coarse dt duration as a function of time
    data = strct["data"]
    plt.plot(data[:,1])
    plt.plot(data[:,1],'r+')
    plt.xlabel('Coarse time step')
    plt.ylabel('Duration in s')
    plt.show()

###for i in `ls run_simu_POGO_M2_2015-05-*` ; do echo $i ; less $i | grep 'Time elapsed' > time_elapsed ; ipy ../../how_to_compute_time_elapsed.txt ; done
