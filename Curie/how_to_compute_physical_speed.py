#!/usr/bin/env python
'''
Usage : 
speed
speed run_simu_bla_bla.log
speed run_simu (will search all files beginning with pattern)

OR

ipy -c "%run ../../how_to_compute_physical_speed.py --log run_simu_POGO_M1_lores_2015-07-11_19h08_111.log"
ipy -c "%run ../../how_to_compute_physical_speed.py"
NB : speed is just a bash function for this command (lazy people...)

OR 

ipy ../../how_to_compute_physical_speed.py
'''
import subprocess
import how_to_compute_time_elapsed
import how_to_compute_fine_dt
import os, re
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--log", dest="log_file", default='',
                  help="Specify log file for which to compute speed", metavar="LOG_FILE")

args = parser.parse_args()

if (args.log_file != ''):  ###Extract interesting data
    subprocess.call("less "+args.log_file+" | grep 'Time elapsed' > time_elapsed", shell=True)
    subprocess.call("less "+args.log_file+" | grep dt= > dt", shell=True)

#Get mean value of Delta_t coarse in s :
delta_t_coarse = (how_to_compute_time_elapsed.coarse_dt())["mean_coarse_dt_less_1LB"]
print 'Delta_t coarse : ', delta_t_coarse, ' s'

#Get mean value of dt_fine in Myrs : (in 14.9 Myrs directly in logfile)
dt_f = how_to_compute_fine_dt.fine_dt()
print 'dt fine : ', dt_f, ' Myrs'

#Compute dt_coarse :
#dt_coarse = dt_f*(product of all nsubcycles) + nremap duration + output creation duration
if (args.log_file != ''):
    outpt_num = re.search('_([0-9]+).log', args.log_file).group(1) ###Extract nml number
    #print outpt_num
    nsub=os.popen("less simu*_"+outpt_num+".nml | grep nsubcycle | awk '{print $1}'").read() ###Extract nsubcycles
    print nsub.strip()
    pdct_nsub = 1
    temp = (nsub.split('='))[-1].split(',')
    for i in temp:
         new = i.split('*')
         pdct_nsub *= int(new[1])**int(new[0]) ###split and multiply correctly
         #print int(new[1]), int(new[0]), int(new[1])**int(new[0])   

else:
    print 'Assumed nsubcycles : 2*1, 6*2' ###Assume fiducial msubcycles
    pdct_nsub = 1**2 * 2**6
dt_coarse = dt_f*pdct_nsub #+ nremap duration + output creation duration... /!\

#Then, compute physical time elapsed in 1 day :
#Delta_t 1d = 24 h/Delta_t coarse * dt_coarse
Delta_t_1d = 86400./delta_t_coarse*dt_coarse

print "Current speed of simulation : {:5.3f} Myr / day".format(Delta_t_1d) ### Somewhat slower because of other effects
