import pythonstartup
import matplotlib as mpl
mpl.use('Agg')
import numpy as np
import os

def plotBHAR(L1,L1_edd,L1_AGN,L1_AGN_edd,namefig):
    #Duration above eddington
    time=L1[:,0]
    time2=L1[:-1,0]
    time2=np.insert(time2,0,0)
    duration=time-time2

    time=L1_AGN[:,0]
    time2=L1_AGN[:-1,0]
    time2=np.insert(time2,0,0)
    duration_AGN=time-time2

    tmin = min([min(L1[:,0]),min(L1_AGN[:,0])])*14.9
    tmax = max([max(L1[:,0]),max(L1_AGN[:,0])])*14.9 #Myr

    #Times and BHARs above eddington
    sup_L1 = L1[L1[:,1]>L1_edd[:,1]]
    dsup_L1 = duration[L1[:,1]>L1_edd[:,1]]
    sup_L1_AGN = L1_AGN[L1_AGN[:,1]>L1_AGN_edd[:,1]]
    dsup_L1_AGN = duration_AGN[L1_AGN[:,1]>L1_AGN_edd[:,1]]

    ##Fraction of time spent above eddington
    ratio_L1=sum(dsup_L1*14.9)/sum(duration*14.9)
    ratio_L1_AGN=sum(dsup_L1_AGN*14.9)/sum(duration_AGN*14.9)

    print ratio_L1*100, ' % of time spent > Eddington for stellar+AGN'
    print ratio_L1_AGN*100, ' % of time spent > Eddington for AGN'

    import matplotlib.pyplot as plt
    fig=plt.figure()
    #plt.plot(L1[:,0]*14.9,L1[:,1],'b,')
    #plt.plot(L1_edd[:,0]*14.9,L1_edd[:,1],'r,')
    #plt.plot(L1_AGN[:,0]*14.9,L1_AGN[:,1],'g,')
    #plt.plot(L1_AGN_edd[:,0]*14.9,L1_AGN_edd[:,1],'r,')
    ##plt.plot(sup_L1[:,0]*14.9,sup_L1[:,1],'r,')
    ##plt.plot(sup_L1_AGN[:,0]*14.9,sup_L1_AGN[:,1],'r,')
    #plt.xlabel('Time [Myr]')

    plt.plot(L1[:,0]*14.9,L1[:,1]/L1_edd[:,1],'g,')
    plt.plot(L1_AGN[:,0]*14.9,L1_AGN[:,1]/L1_AGN_edd[:,1],'r,')
    plt.plot([tmin-0.05*(tmax-tmin),tmax+0.05*(tmax-tmin)],[1,1],'black')

    plt.text(tmin+0.001*(tmax-tmin),0.31*min(L1_AGN[:,1]),str(int(round(ratio_L1*100)))+' % of time spent > Eddington for stellar+AGN',color='g')
    plt.text(tmin+0.001*(tmax-tmin),0.09*min(L1_AGN[:,1]),str(int(round(ratio_L1_AGN*100)))+' % of time spent > Eddington for AGN',color='r')

    plt.xlabel('Time [Myr]')
    plt.ylabel('BHAR/Eddington rate')
    plt.yscale('log')
    plt.xlim([tmin-0.05*(tmax-tmin),tmax+0.05*(tmax-tmin)])
    plt.tight_layout(True)
    plt.savefig(namefig)
    print namefig, ' has been created.'


os.chdir('/ccc/scratch/cont003/dsm/rooso/POGO_lores/L1')
L1=np.genfromtxt('BHAR_L1_045-386.txt') #Time, BHAR
L1_edd=np.genfromtxt('EDDINGTON_L1_045-386.txt')
L1_AGN=np.genfromtxt('../L1_AGN/BHAR_L1_AGN_052-298.txt')
L1_AGN_edd=np.genfromtxt('../L1_AGN/EDDINGTON_L1_AGN_052-298.txt')
plotBHAR(L1,L1_edd,L1_AGN,L1_AGN_edd,'BHAR_L1_L1_AGN_new.png')

os.chdir('/ccc/scratch/cont003/dsm/rooso/POGO_lores/L2_KThR+AGN')
L2=np.genfromtxt('BHAR_L2_89-268.txt') #Time, BHAR
L2_edd=np.genfromtxt('EDDINGTON_L2_89-268.txt')
L2_AGN=np.genfromtxt('../L2_AGN/BHAR_L2_AGN_89-258.txt')
L2_AGN_edd=np.genfromtxt('../L2_AGN/EDDINGTON_L2_AGN_89-258.txt')
plotBHAR(L2,L2_edd,L2_AGN,L2_AGN_edd,'BHAR_L2_L2_AGN_new.png')

os.chdir('/ccc/scratch/cont003/dsm/rooso/POGO_lores/LLL3')
LLL3=np.genfromtxt('BHAR_LLL3_390-416.txt') #Time, BHAR
LLL3_edd=np.genfromtxt('EDDINGTON_LLL3_390-416.txt')
LLL3_AGN=np.genfromtxt('../LLL3_AGN/BHAR_LLL3_AGN_390_410.txt')
LLL3_AGN_edd=np.genfromtxt('../LLL3_AGN/EDDINGTON_LLL3_AGN_390_410.txt')
plotBHAR(LLL3,LLL3_edd,LLL3_AGN,LLL3_AGN_edd,'BHAR_LLL3_LLL3_AGN_new.png')
