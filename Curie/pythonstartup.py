#!/usr/bin/env python
'''
file created bec. somehow .pythonstartup not loaded when Using Lets_POGO.sh
'''

import matplotlib

matplotlib.rcParams['ytick.labelsize'] ='large'
matplotlib.rcParams['xtick.labelsize'] ='large'
matplotlib.rcParams['axes.labelweight'] = 'roman'
matplotlib.rcParams['lines.linewidth'] = 2.
matplotlib.rcParams['font.weight'] = 'roman'
matplotlib.rcParams['font.size'] = 18#14
matplotlib.rcParams['axes.linewidth'] = 2#1.5

