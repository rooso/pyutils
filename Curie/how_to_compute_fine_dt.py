#!/usr/bin/env python

#1. from RAMSES logfile : cat logfile | grep 'dt=' > dt

#2. in Python :
import numpy as np

def fine_dt():
    #Old stuff
    #data = np.genfromtxt('dt',usecols=(5)) ###Extract data
    ##print np.mean(data[:])
    ##test = np.genfromtxt('dt')
    ##print test[0,:]
    #if np.isnan(np.mean(data[:])):  ### OR : if (np.isnan(data)).any():   ### Check if data read correctly
    #    #print 'Inside if'
    #    data = np.genfromtxt('dt',usecols=(6)) ###If not, try again (pb with column numbers : transition dt= 9... / dt=10... )

    with open ('dt') as source:
        a = [line.split() for line in source]

    data = []
    for i in xrange(0, len(a)):
        j = 0
        while (str(a[i][j]) != str('dt=')):
            j += 1
        data.append(float(a[i][j+1]))

    #Mean value of dt fine in Myrs :
    return np.mean(data[:])*14.9

if __name__ == "__main__":
    #Mean value of dt fine in Myrs :
    print 'Mean value of dt fine in Myrs :', fine_dt()
