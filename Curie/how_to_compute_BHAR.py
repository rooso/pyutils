#Run this command : logfile_extract run_simu_POGO_M1_lores_2015-09-03_13h56_163.log accretion > accretion.txt
#accretion.txt contains:
#Walltime Time Id Mass(Msol)    Bondi(Msol/yr)   Edd(Msol/yr)              x              y              z          Delta_mass

import matplotlib as mpl
mpl.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser

#argument and option parser
parser = ArgumentParser()
parser.add_argument("--lookup", dest="lookup", default=False, action='store_true',
                  help="Output lookup file for accretion")
parser.add_argument("--eddington", dest="eddington", default=False, action='store_true',
                  help="Output eddington file for accretion")
args = parser.parse_args()

data = np.genfromtxt('accretion.txt',usecols=(1,3,4,5,9))

if args.eddington:
    DataOut = np.column_stack((data[:,0],data[:,3]))
    np.savetxt('eddington.txt', DataOut)
    print 'eddington.txt created.'
elif args.lookup:
    DataOut = np.column_stack((data[:,0],data[:,2]))
    np.savetxt('accretion_lookup.txt', DataOut)
    print 'accretion_lookup.txt created.'
else:
    f, (ax1, ax2) = plt.subplots(2, sharex=True)
    ax1.plot(data[:,0]*14.9,data[:,2]) #BHAR
    ax1.plot(data[:,0]*14.9,data[:,3], 'r') #Eddington
    ax1.set_xlabel('Time in Myrs')
    ax1.set_ylabel('BHAR in Msun/yr')
    ax1.set_yscale('log')
    
    EddBHAR = data[:,3]*0
    dummy=0
    cum_delta = 0
    cum_masses = 0
    for idx, mass in enumerate(data[:,1]):
        if idx>0:
            #if (np.abs(data[idx,1]-data[idx-1,1]) < 1e-2): 
            #    print 'Mass i - mass i-1 :', data[idx,1]-data[idx-1,1]
            #    print 'Delta_m :', data[idx,4]
            #    dummy += 1
            cum_masses += data[idx,1]-data[idx-1,1]
  
        #Eddington-limited BHAR :
        EddBHAR[idx] = min(data[idx,2],data[idx,3])

    #print 'There are ',  dummy, 'weird Delta_masses over ', len(data[:,1])-1
    print 'Cumulative BHAR :', sum(data[:,2]), 'Msun/yr.'
    print 'Cumulative Edd-limited BHAR :', sum(EddBHAR), 'Msun/yr.'
    print 'Cumulative Delta_mass : ', sum(data[1:,4]), 'Msun.' #M_0 inconnu -> commencer a Delta_m_1
    print 'Cumulative mass i - mass i-1 :', cum_masses
    print 'Mass range : ', data[0,1], data[-1,1], 'Msun.'
    print 'Time range : ', data[0,0]*14.9, data[-1,0]*14.9, 'Myrs.'
 
    ax1.plot(data[:,0]*14.9,EddBHAR,'g--')
    ax2.plot(data[:,0]*14.9,data[:,4]) #Delta_mass 
    ax2.set_ylabel('Delta mass in Msun')#NB : Mass(i-1)+Delta_mass(i) = Mass(i)
    ax2.set_yscale('log')
    plt.savefig('accretion.png')
    plt.show()
