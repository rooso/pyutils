#!/usr/bin/env python
'''###########################################################################
   #### This program plots the LOPs located in LOP_sample in 3D           ####
   #### and shows their density profile                                   ####
   ####                                                                   ####
   ###########################################################################
'''
import numpy as np
import pythonstartup

file = '/Users/oroos/Post-stage/LOP_sample/list_of_lops.txt'
x, y, z = np.genfromtxt(file,skip_header=3)
