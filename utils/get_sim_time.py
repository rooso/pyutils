#!/usr/bin/env python

def get_sim_time(output_number, option=0, string=True):
    '''
        get_sim_time: get_sim_time(output_number, option=0, string=True)
        
        Read sim time from :
            - sim_times.txt
            - time given in info_XXXXX.txt
            - time in fits.gz file
            
        
        Parameters:
            output_number: number of the RAMSES output for which to search time
            option: option used to search time
                0: search sim_times.txt
                1: ...
            string: return time as string. Default is True
        
        Output:
            simulation time, flag. Flag is True if time has been successfully found.
    
    '''
    import os, sys
    import numpy as np
    import astropy.units as U
    from astropy.io import fits as pyfits

    ok = False
    time=[]
    
    dir = os.getcwd()+'/'
    if (option == 0 and os.path.isfile(dir+'sim_times.txt')):
        data_time = np.genfromtxt(dir+'sim_times.txt', skip_header=1)
        times = data_time[:,1]*U.Myr
        outputs = data_time[:,0]
        time = times[np.where(outputs == float(output_number))] #Myr
        
    elif (option == 1 and os.path.isfile(dir+'output_'+output_number+'/info_'+output_number+'.txt')):
        import subprocess
        fname = dir+'output_'+output_number+'/info_'+output_number+'.txt'
        p = subprocess.Popen(['cat', fname, '|', 'xargs', 'grep', '-l', 'time'], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        result, err = p.communicate()
        if p.returncode != 0:
            raise IOError(err)
        time = [float(result.strip().split()[-1])*14.9] #Myr
        
    #add other options here
    elif (option == 2):
        for file in os.listdir(dir):
            if (file.endswith(".fits.gz") or file.endswith(".fits")) and output_number in file:
                data, hdr = pyfits.getdata(file, header=True)
                time = [hdr['time']] #Myr
                break
        
    else:
        time=[]
    
    if len(time) > 1:
        print 'More than 1 time !'
        print time
        sys.exit(2)
    elif len(time) == 0:
        print 'Time not found !'
        time = [-42] #consider another option
    else:
        ok = True
    if (string):
        return str("%0.2f" % time[0]), ok
    else:
        return float("%0.2f" % time[0]), ok

        
if __name__ == "__main__":
    print "Execution de get_sim_time.py"
