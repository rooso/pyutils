#!/usr/bin/env python
'''
    find_colors: find_colors(param, interval, cmap='jet')
    
    This function scales a color map on given parameter.
    
    Parameters:
        param: variable for which to compute color scale
        interval: interval on which to compute color scale
        cmap: colormap. Default is 'jet'.
    
    Output:
        color array
'''
    

def find_colors(param, interval, cmap='jet', cbar=False, ax=None):
    import matplotlib.pyplot as plt
    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    import numpy as np

    length = len(param)
    colormap = cm = plt.get_cmap(cmap) 
    cNorm  = colors.Normalize(vmin=min(interval), vmax=max(interval))
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=colormap)
    #print scalarMap.get_clim()
    colorVal = []

    for idx in range(length): 
        colorVal.append(scalarMap.to_rgba(param[idx]))

        #if cbar:
        # scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=colormap)
        #  plt.colorbar(colorVal,ax=ax)

    return colorVal

if __name__ == "__main__":
    print "Execution de find_colors.py"
