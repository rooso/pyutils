#!/usr/bin/env python

def jeans_correction(rho, T, boxlen=200., lmax=16, T_thr=100.):
    '''
        jeans_correction: jeans_correction(rho, T, boxlen=200., lmax=16, T_thr=100.)
       
        Correction of Jeans' Polytrope for RAMSES sims
       
        Parameters:
            rho: gas hydrogen density in H/cc
            T: gas temperature before correction in K
            boxlen: size of the RAMSES box in kpc. Default is 200
            lmax: maximum level of refinement. Default is 16
            T_thr: temperature threshold in K. Default is 100
    
        Output:
            corrected temperature
                                                    
    '''

    import numpy as np

    alpha = (0.041666*np.sqrt(32*np.pi)*(boxlen*1e3/2**lmax)**2) #boxlen in kpc
    rho_0 = T_thr/alpha            #rho(T = 100 K) H/cc
    #print type(rho).__name__
    if type(rho).__name__ != 'float':
        ###t_rho_0 = [rho >= rho_0]
        T[(rho >= rho_0) & (T <= 2.*alpha*rho)] = T_thr
    else:
        if (rho >= rho) and (T <= 2.*alpha*rho):
            T = T_thr

    return T

if __name__ == "__main__":
    print "Execution de jeans_correction.py"

