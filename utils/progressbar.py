#!/usr/bin/env python
'''
This function prints a dynamic progress bar of a loop.

Parameters :
     i : current index
     end_val : the maximal index to reach
     bar_length : bar size [optional]

'''
import sys

def progressbar(i, end_val, bar_length=20):
    percent = float(i) / end_val
    hashes = '#' * int(round(percent * bar_length))
    spaces = ' ' * (bar_length - len(hashes))
    sys.stdout.write("\rPercent: [{0}] {1}%".format(hashes + spaces, int(round(percent * 100))))
    sys.stdout.flush()
