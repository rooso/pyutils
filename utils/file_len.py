#!/usr/bin/env python
'''
    file_len: file_len(fname)
    
    Count number of lines in specified file using bash 'wc -l'

    Parameters:
        fname: file name
    
    Output:
        number of lines
'''
def file_len(fname):
    import subprocess 
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE, 
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

if __name__ == "__main__":
    print "Execution de file_len.py"