#!/usr/bin/env python

def get_labels(header, threeD=False):
    '''
    get_labels: get_labels(header, threeD=False)
    
    Read header of FITS file to find x, y (and z) labels.
    
    Parameters:
        header: header in which to search
        threeD: return 3 labels instead of 2. Default is False.
    
    Output:
        x, y (and z) labels
    '''
    i_dim, j_dim, k_dim = header['IDIM'], header['JDIM'], header['KDIM']
    #print i_dim, j_dim, k_dim

    if (threeD):
        dim = 3
    else:
        dim = 2

    labs = []
    for i in range(dim):
        if i_dim == i+1:
            labs.append('x')
        if j_dim == i+1:
            labs.append('y')
        if k_dim == i+1:
            labs.append('z')
    #print labs, i+1, dim
    return labs


if __name__ == "__main__":
    print "Execution de get_labels.py"
