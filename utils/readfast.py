#!/usr/bin/env python
'''
Fast algorithm (with low memory impact) to read very big files.
'''
import numpy as np

def iter_loadtxt(filename, delimiter=' ', skiprows=0, dtype='float'):
    def iter_func():
        with open(filename, 'r') as infile:
            for _ in range(skiprows):
                next(infile)
            for line in infile:
                line = line.rstrip().split(delimiter)
                #print line, len(line)
                #for item in line:
                    #print item
                    #yield dtype(item)
        iter_loadtxt.rowlength = len(line)

    data = np.fromiter(iter_func(), dtype=dtype)
    data = data.reshape((-1, iter_loadtxt.rowlength))
    return data

#data = iter_loadtxt('large_text_file.csv')
