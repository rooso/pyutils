#!/usr/bin/env python

def get_dir_name():
    '''
        get_dir_name : get_dir_name()
        
        Gets current working directory name without absolute path
            
        Parameters:
            None
        
        Output:
            directory name
    
    '''
    import os
    dir = os.getcwd()+'/'
    #dir = '/Users/oroos/Post-stage/SIMUS/Kinthermrad_lores24pc/'
    if "OGO" in dir: #POGO or LOGO for lores
        junk, Users, oroos, Post_stage, simus, pogo_name, dir_name, junk2 = dir.split('/')
    else:
        junk, Users, oroos, Post_stage, simus, dir_name, junk2 = dir.split('/')
        
    return dir_name
