#!/usr/bin/env python

def get_boxlen_lmax(output_number, ncells=0, option=0, test=''):
    '''
    get_boxlen_lmax: (output_number, ncells=0, option=0, test='')
    
    Read boxlen and lmax in statitstics_output_number.ascii
    if it exists, else from the prompt
    Other options still to implement
    
    Paramters:
        output_number : number of the RAMSES output to search
        ncells : number of cells in the output. Default is 0.
        option : option with which to search boxlen and lmax. Default is 0.
            0: search statistics_output_number.ascii and check ncells
            1: enter from prompt
    
    Output:
        boxlen [kpc] and lmax
    
    '''
    import os, sys
    import file_len
    import numpy as np
    import astropy.units as U
    dir = os.getcwd()+'/'
    stat_file = dir+'statistics_'+output_number+test+'.ascii'
    if output_number == 'mean':
        for f in os.listdir(dir):
            if f.startswith("statistics") and f.endswith(".ascii"):
                stat_file = f
    #print stat_file
    if os.path.isfile(stat_file) and option==0:
        data_stat = np.genfromtxt(stat_file)
        boxlen = data_stat[1]*U.kpc #kpc
        lmax = data_stat[2]
        if ncells == 0:
            part_file = dir+'gas_part_'+output_number+test+'.ascii'
            if os.path.isfile(part_file):
                print 'Counting cells...'
                ncells = file_len.file_len(part_file)
        if data_stat[0] != ncells and ncells != -1: 
            print 'Number of cells does not match !'
            print data_stat[0], ncells
            sys.exit(2)
    else:
        boxlen = float(raw_input('Enter boxlen in kpc : '))*U.kpc
        lmax = int(raw_input('Enter lmax : '))

    return boxlen, lmax

if __name__ == "__main__":
    print "Execution de get_boxlen_lmax.py"
