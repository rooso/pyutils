#!/usr/bin/env python

def do_hist2d(x, y, x_bins, y_bins, set_log=True, mask=0, weight=None):
    '''
    Do_hist2d : do_hist2d(x, y, x_bins, y_bins, set_log=True, mask=0, weight=None)
    
    Create a 2d histogram from input values
    
    Parameters:
        x,y: variables on which to compute 2d histogram
        x_bins,y_bins: bins
        set_log: take log of histogram. Default is True
        mask: values to mask. Default is 0.
        weight: weight of the histogram. Default is None
    
    Output:
        2d histogram and actual bins
    
    '''
    import numpy as np

    H, x_edges, y_edges = np.histogram2d(x,y,bins=[x_bins,y_bins], weights=weight)
    # H needs to be rotated and flipped (x <-> y)
    H = np.rot90(H)
    H = np.flipud(H)
 
    # Mask zeros
    if mask != None:
        H = np.ma.masked_where(H==mask,H) # Mask pixels with a value of zero
    if (set_log):
        H = np.log10(H)
        
    return H, x_edges, y_edges


if __name__ == "__main__":
    print "Execution de do_hist2d.py"
