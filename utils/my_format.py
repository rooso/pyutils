def format_precision(precision, value):
    """
        my_format: format_precision(precision, value)
        
        Format a value with the given number of digits in scientific notation
        
        Parameters:
            precision: number of digits of the output
            value: number to format
        
        Output:
            formatted string with specified precision in scientific format
    
    """
    p = precision - 1 # the format description takes the number of digits after the point
    format_str = "{{:.{}e}}".format(p) # the format string is something like "{:.3e}"
    return format_str.format(value)

if __name__ == "__main__":
    print "Execution de my_format.py"