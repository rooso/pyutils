def get_unit(header):
    '''
        get_unit: get_unit(header)
        
        Search unit in header of FITS file
        
        Parameters:
            header: header of FITS file in which to search unit
        
        Output:
            unit
        
    '''
    unit = header['*unit*']
    unit = (str(unit).split("'"))[1]
    unit = unit.strip()
    if len(unit) > 0:
        unit = ' ['+unit+']'
    else:
        if 'level' not in header['outval']: 
            print 'Unit error !'
        unit = ''
    return unit


if __name__ == "__main__":
    print "Execution de get_unit.py"
