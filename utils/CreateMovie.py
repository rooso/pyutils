#!/usr/bin/env python

"""
    CreateMovie(plotter, numberOfFrames, type, fps=24, skip_files=True, no_movie=False)
    
    This function creates a movie called movie.mp4 in the current working directory.
    
    Parameters:
    	plotter: This parameter is a function of the following form:
    				def plotter(frame_number)
    			 where frame_number is the current frame that needs to be plotted
    			 using the matplotlib.pyplot library.
    	numberOfFrames: The total number of frames in the movie.
    	fps: The frames per second. The default is 24
        skip_files: skip existing _tmp*.jpg. Default is True. _tmp*.jpg are temporary files used to build the movie
        no_movie: create temporary files but not the movie itself. Default is False
    	
    Output:
    	The function will create a movie called movie.mp4. Make sure that you don't
    	have any files called movie.mp4 in the current working directory because they will be deleted.
"""

def CreateMovie(plotter, numberOfFrames, type, fps=24, skip_files=True, no_movie=False):
    import os, sys
    import matplotlib.pyplot as plt

    for i in range(numberOfFrames):
        fname = '_'+type+'_tmp%05d.jpg'%i
        if os.path.isfile(fname) and skip_files:
            print fname, ' already exists, skipping...'
        else:
            plotter(i, type)
		
            plt.savefig(fname)
            plt.clf()

    if not no_movie:
        #os.system("rm movie_"+type+".mp*")
        #os.system("ffmpeg -y -f image2 -i _"+type+"_tmp%05d.png movie_"+type+".mpg")
        os.system("ffmpeg -y -r "+str(fps)+" -i _"+type+"_tmp%05d.jpg movie_"+type+".mp4")
        #os.system("rm _"+type+"_tmp*.jpg")

#for curie : /ccc/cont003/home/dsm/rooso/bin/ffmpeg (if needed)

if __name__ == "__main__":
    print "Execution de CreateMovie.py"
