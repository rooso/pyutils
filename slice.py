#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
######################################################
###       This code was written by Marc Joos       ###
###       and later modified by Orianne Roos       ###
######################################################
'''
import pythonstartup
import sys, os
import getopt
import matplotlib as mpl
mpl.use('pdf')
import pymses as pm
import numpy  as np
import pylab  as pl
import math   as ma
from pymses.utils import constants as ct
from pymses.filters import CellsToPoints
from pymses.analysis.visualization import Camera, SliceMap, ScalarOperator
from matplotlib.colors import LogNorm
from argparse import ArgumentParser

bold  = "\033[1m"
reset = "\033[0;0m"

def getNumFile(filerep):
    filename = np.sort(glob.glob(filerep + "output*"))
    nfiles   = len(filename)
    numfile  = []
    for i in range(nfiles):
        nameparse = filename[i].split("_")
        numfile.append(int(nameparse[nameparse.__len__()-1]))
    return(nfiles,numfile)

def parseDirDisk(filedir):
    nameparse = filedir.split("_")
    nparse    = len(nameparse)
    alpha     = nameparse[nparse-1]
    mag       = nameparse[nparse-2]
    return(mag,alpha)

def centerOfMass(d,coord,mass,factor):
    ind = pl.find(d > max(d)/factor)
    x = sum(mass[ind]*coord[ind,0])/sum(mass[ind])
    y = sum(mass[ind]*coord[ind,1])/sum(mass[ind])
    z = sum(mass[ind]*coord[ind,2])/sum(mass[ind])
    return np.array([x,y,z])

def main():

    parser = ArgumentParser()
    parser.add_argument("-p", "--path", dest="path", default='/Users/oroos/Post-stage/SIMUS/Test_agn/',
              help="path of directory containing the outputs", metavar="PATH")
    parser.add_argument("-d", "--directory", dest="directory", default='output/',
              help="directory of the outputs", metavar="DIRECTORY")
    parser.add_argument("-o", "--output", dest="output", default=681, type=int,
              help="number of the output to plot", metavar="OUTPUT")
    parser.add_argument("-v", "--vel", dest="vel", default=False, action='store_true',
              help="if 'True', plot velocity field")
    parser.add_argument("-f", "--vel-factor", dest="vel_factor", default=0.1, type=float,
              help="change velocity scale (default is 10^4 km/s)", metavar="VEL-FACTOR")
    parser.add_argument("-s", "--size", dest="size", default=[0.12,0.12], type=float, nargs=2,
              help="size of the slice in [0,1]", metavar=["SIZE1","SIZE2"])
    parser.add_argument("-S", "--map-size", dest="map_size", default=1024, type=int,
              help="maximum number of pixels on the map", metavar="MAP-SIZE")
    parser.add_argument("-l", "--los", dest="los", default='x',
              help="Line-of-sight {x,y,z}", metavar="x|y|z")
    parser.add_argument("-m", "--minimum", dest="minimum", default=1e-7, type=float,
              help="minimum value for the colormap", metavar="MINIMUM")
    parser.add_argument("-M", "--maximum", dest="maximum", default=1e6, type=float,
              help="maximum value for the colormap", metavar="MAXIMUM")
    parser.add_argument("-r", "--record", dest="record", default=False, action='store_true',
              help="save the plot")

    args = parser.parse_args()
        #type = options.type
        #show_labels = options.show_labels

    if(args.los == "x"):
        up = "y"
        vx = 2
        vy = 1
        labelx = "z [kpc]"
        labely = "y [kpc]"
    elif(args.los == "y"):
        up = "z"
        vx = 0
        vy = 2
        labelx = "x [kpc]"
        labely = "z [kpc]"
    else:
        up = "y"
        vx = 0
        vy = 1
        labelx = "x [kpc]"
        labely = "y [kpc]"

    filerep  = args.path + args.directory
    #nfiles,numfile = getNumFile(filerep)
    nfiles = 1
    numfile = args.output
    if(args.record):
        nameparse = filerep.split("/")
        savedir   = ""
        mag,alpha = parseDirDisk(args.directory)
        savedir   = args.path#checkDir(savedir, "mu" + mag)

    # Define the file and fields to read
    ro     = pm.RamsesOutput(filerep, args.output)
    source = ro.amr_source(["rho","vel"])
    time   = ro.info["time"]*ro.info["unit_time"].express(ct.Myr)

    # Slice in density
    # Center
    #csource = CellsToPoints(source)
    #    cells   = csource.flatten()
    #    nc      = cells.npoints
    #    cp      = cells.points*ro.info["unit_length"].express(ct.cm)
    #    dx      = cells.get_sizes()*ro.info["unit_length"].express(ct.cm)
    #    d       = cells.fields["rho"]
    #    v       = cells.fields["vel"]*ro.info["unit_velocity"]\
    #              .express(ct.km/ct.s)
    #mass    = d*ro.info["unit_density"].express(ct.g/ct.cm**3)*dx**3
    #    centerm = centerOfMass(d,cp,mass,100)/ro.info["unit_length"].express(ct.cm)
    #    cc      = cp - centerm
    
    xc,yc,zc = [0.5,0.5,0.5]#centerm
    # Width of the region to plot
    xr,yr = args.size[0],args.size[1]
    xbl =  (- xr/2.*ro.info["unit_length"]).express(ct.kpc)
    xbr =  (+ xr/2.*ro.info["unit_length"]).express(ct.kpc)
    ybl =  (- yr/2.*ro.info["unit_length"]).express(ct.kpc)
    ybr =  (+ yr/2.*ro.info["unit_length"]).express(ct.kpc)
    extent = [xbl,xbr,ybl,ybr]
    cam = Camera(center=[xc,yc,zc] \
                 , line_of_sight_axis=args.los, region_size=[xr, yr] \
                 , up_vector=up, map_max_size=args.map_size, log_sensitive=True)
    rho_op = ScalarOperator(lambda dset: dset["rho"]*ro.info["unit_density"].express(ct.H_cc)) #dset["vel"][:,2]
    smap   = SliceMap(source, cam, rho_op, z=0.0)

    pl.figure(figsize = (6,4))
    if(args.vel):
        p = cam.get_slice_points(0.0)
        nx, ny = cam.get_map_size()
        dset = pm.analysis.sample_points(source, p)
        vel  = dset["vel"]*ro.info["unit_velocity"].express(ct.km/ct.s)*args.vel_factor
        rs = 20 #how many points to skip ?
        x = np.linspace(xbl,xbr,nx)
        y = np.linspace(ybl,ybr,ny)
        u,v = np.zeros((nx,ny)),np.zeros((nx,ny))
        for i in range(nx):
            for j in range(ny):
                if(i%rs == 0):
                    if(j%rs == 0):
                        u[i,j] = vel[:,vx].reshape(nx,ny)[i,j]
                        v[i,j] = vel[:,vy].reshape(nx,ny)[i,j]
                    else:
                        u[i,j] = 'Inf'
                        v[i,j] = 'Inf'
                else:
                    u[i,j] = 'Inf'
                    v[i,j] = 'Inf'
        if(args.los == "z"):
            q = pl.quiver(x,y,u.transpose(),v.transpose(), units='x', scale=1*nx)
        else:
            q = pl.quiver(x,y,-u.transpose(),v.transpose(), units='x', scale=1*nx)
        qk = pl.quiverkey(q, 0.2, 0.1, 1e3, r"%10.1E km/s"% (1e3/args.vel_factor), fontproperties={'weight': 'semibold'}) #km s$^{-1}$

    # Plot your slice
    pl.title("t = %.0f Myr" %time)
    pl.xlabel(labelx)
    pl.ylabel(labely)
    pl.imshow(smap.transpose(),origin='lower',extent=extent \
              , vmin = args.minimum, vmax = args.maximum, norm=LogNorm(vmin=args.minimum,vmax=args.maximum), cmap='jet_r')
    cbarx = pl.colorbar()
    cbarx.set_label(r"log($n$) (cm$^{-3}$)")
    if(args.record):
        for format in ["png","pdf"]:
            namefig = savedir + "test"+"_t%05d." %time + format #quiver key does not appear in png !!
            print(bold + "Save figure: " + reset + namefig)
            pl.savefig(namefig)
        pl.close()
    else:
        pl.show()

if __name__ == "__main__":
    main()
