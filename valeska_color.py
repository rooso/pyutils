#!/usr/bin/env python
'''
Small code for Valeska. Create nice color bar and curves with color map.    
'''

import utils.find_colors
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

fig = plt.figure(figsize=(10, 6.5))

x = np.linspace(0,10)
z = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
label=[(str(zz)+' kpc') for zz in z]
handles=[]

colorVal = utils.find_colors(z, [min(z), max(z)], cmap='jet_r')

for idx,lab in enumerate(label):
     p_out=plt.plot(x,np.sin(x+idx),marker='o', color=colorVal[idx], label=lab, linewidth=2)
     handles.append(mlines.Line2D([], [], label=lab, linewidth=4, color=colorVal[idx]))

plt.legend(handles=handles, fontsize=11)
plt.show()
