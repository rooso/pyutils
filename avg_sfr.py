#!/usr/bin/env python
'''
Read output from :
%run ~/python/Post-stage/plot_sfr part_00XXX.dat -eta_sn=0.2 -f_w=10 -out

and compute avg SFR from specified time 

'''
import pythonstartup
import numpy as np
from argparse import ArgumentParser
from matplotlib.patches import Rectangle

#argument and option parser
parser = ArgumentParser()
parser.add_argument("inp", 
                  help="part_00XXX.dat.sfr", metavar="FILE")
parser.add_argument("-t", "--avg-time", dest="avg_time", default=0, type=float, 
                  help="Average SFR between TIME and NOW.", metavar="TIME")
args = parser.parse_args()

data = np.genfromtxt(args.inp)

data_prime=data[data[:,0]>args.avg_time]
print 'Average SFR since t=', args.avg_time, 'Myrs is :', np.mean(data_prime[:,1]), 'Msun/yr.'
