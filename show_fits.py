#!/usr/bin/env python
'''
################################################################
#### This program plots fits.gz files generated by amr2map. ####
####                                                        ####
#### /!\ implementation of vector field in progress         ####
####                                                        ####
#### NB : on Curie, SymLogNorm is not recognized.           ####
################################################################
'''
import pythonstartup
from mpl_toolkits.axes_grid1 import make_axes_locatable
from astropy.io import fits as pyfits
from pylab import quiver
import matplotlib.pyplot as plt
from matplotlib import ticker
from matplotlib.colors import LogNorm
from matplotlib.colors import SymLogNorm #comment for Curie
from matplotlib.colors import Normalize
import sys, os, os.path, math, getopt
import utils.get_labels
import utils.get_dir_name
import numpy as np
from argparse import ArgumentParser
from matplotlib.patches import Rectangle

#argument and option parser
parser = ArgumentParser()
parser.add_argument("output", nargs='+',
                  help="one or more output numbers, or 'all' for all outputs", metavar="OUTPUT")
parser.add_argument("-t", "--type", dest="type", default='rho',
                  help="type of variable to plot : rho (default), T, v_[x|y|z], level, or all", metavar="TYPE")
parser.add_argument("-l", "--show-labels", dest="show_labels", default=False, action='store_true',
                  help="show labels on the plot")
parser.add_argument("--no-bh", dest="show_bh", default=True, action='store_false',
                  help="do not show BH location on the plot")
parser.add_argument("--dont-sharex", dest="dont_sharex", default=False, action='store_true',
                  help="do not share x axis")
parser.add_argument("--dont-sharey", dest="dont_sharey", default=False, action='store_true',
                  help="do not share y axis")
parser.add_argument("--no-vel", dest="vel", default=True, action='store_false',
                  help="do not show velocity field on the plot")
parser.add_argument("--vel-factor", dest="vel_factor", default=5, type=float, #FOR NICE LARGE VIEW, SHOULD PUT 4 !!!
                  help="multiplicative factor for arrow size", metavar="VEL-FACTOR")
parser.add_argument("--vel-skip", dest="skip", default=200, type=int, #FOR NICE LARGE VIEW, SHOULD PUT 100 !!!
                  help="only print subset of arrows", metavar="SKIP")
parser.add_argument("--key-length", dest="key_length", default=1e3, type=float,
                  help="length of key arrow (in km/s)", metavar="KEY-LENGTH")
parser.add_argument("--disk", dest="disk", default=False, action='store_true',
                  help="face-on view")
parser.add_argument("--large", dest="large", default=False, action='store_true',
                  help="large view")
parser.add_argument("--side", dest="side", default=False, action='store_true',
                  help="side view")
parser.add_argument("--silent", dest="silent", default=False, action='store_true',
                  help="do not print messages")

args = parser.parse_args()
if (args.large):
    #change default value of vel_skip and vel_factor
    parser.set_defaults(skip=100, vel_factor=20)
    #print parser.get_default('skip'), parser.get_default('vel_factor')

args = parser.parse_args()
output = args.output
type = args.type
show_labels = args.show_labels
show_bh = args.show_bh
sharex = not args.dont_sharex
sharey = not args.dont_sharey
vel = args.vel
vel_factor = args.vel_factor
key_length = args.key_length
skip = args.skip
disk = args.disk
large = args.large
side = args.side
silent = args.silent

#Get figure name
figname = 'fits_'+str(output[0])+'_'+type
if (disk):
    figname += '_disk'
if (large):
    figname += '_large'
if (side):
    figname += '_side'
if (not vel):
    figname += '_no-vel'

#show_labels = True #True/False
#type = 'level' #v_z/rho/T/level
col_map = ['jet_r']

#Handle variable type(s) to plot
type = [type]
if 'v_' in type[0]:
    ###if (abs):
        ###
    ###else:
    my_norm = [SymLogNorm(1, vmin=-1e4, vmax=1e4)]
    col_map = ['RdBu']
    #comment above for Curie
    #my_norm = [LogNorm(vmin=1, vmax=1e4)]
elif 'T' in type[0]:
    my_norm = [LogNorm(vmin=1e2, vmax=1e10)]
    col_map = ['jet']
elif 'P' in type[0]:
    my_norm = [LogNorm(vmin=1e-6, vmax=1e6)]
    col_map = ['jet_r']
elif 'rho' in type[0]: #'rho' is the default
    my_norm = [LogNorm(vmin=1e-6, vmax=1e5)]
elif 'level' in type[0]:
    my_norm = [Normalize(vmin=9, vmax=17)]
#add new types here :
#elif type == ['new']:
#    my_norm = [Normalize(vmin=new_vmin, vmax=new_vmax)]
elif type == ['all']:
    type = ['rho', 'T', 'v_z', 'level'] #improve to search everything automatically ??
    my_norm = [LogNorm(vmin=1e-7, vmax=1e7), LogNorm(vmin=1e2, vmax=1e10), SymLogNorm(1, vmin=-1e4, vmax=1e4), Normalize(vmin=9, vmax=17)]
    col_map = ['jet_r', 'jet', 'RdBu', 'jet_r']
    #comment above for Curie
    #my_norm = [LogNorm(vmin=1e-7, vmax=1e7), LogNorm(vmin=1e2, vmax=1e10), LogNorm(vmin=1, vmax=1e4), Normalize(vmin=9, vmax=17)]
    #col_map = ['jet_r', 'jet', 'jet_r', 'jet_r']
else :
    my_norm = [LogNorm()]
    print 'Caution, unknown type !'

if disk:
    for idx,typ in enumerate(type):
        type[idx] = typ+'_disk'
if large:
    for idx,typ in enumerate(type):
        type[idx] = typ+'_large'
if side:
    for idx,typ in enumerate(type):
        type[idx] = typ+'_side'

#If requested, search all FITS.gz files in directory
if output[0] == 'all':
    output = []
    dir = os.getcwd()+'/'
    for file in os.listdir(dir):
         if file.startswith('map') and file.endswith(type[0]+".fits.gz"):
            end = -12 - (len(type[0])-3)
            output.append(file[4:end])

output.sort() #files need to be sorted on Curie
ctr = 0

#prepare the plot
row = int(math.ceil(len(output)*len(type)/4.))
col = min(len(output)*len(type),4)

x_fig, y_fig = 35, 5.6*row
#x_fig, y_fig = 18, 3.8*row
x_key, y_key = 0.2, 0.015
rect_wdth, rect_ht = 0.32, 0.23
if col*row == 1:
    x_fig, y_fig = 8, 8 #10, 10
    x_key, y_key = 0.08, 0.015
    rect_wdth, rect_ht = 0.24, 0.15

#print col, row
fig, axarr = plt.subplots(row, col, sharex=sharex, sharey=sharey, figsize=(x_fig,y_fig))
if col*row > 1:
    #print axarr.size
    axarr=axarr.reshape(axarr.size)
    #print axarr.shape
else:
    axarr = [axarr]
fig.subplots_adjust(wspace=0.38, left=0.05, right=0.95, hspace=0.3, top=0.8)

#print output, type

for idx, outpt in enumerate(output):
    for idx_typ, typ in enumerate(type): #If requested, search for all types
        if os.path.isfile('map_'+outpt+'_'+typ+'.fits.gz'): #check existence of file first
            
            if not silent:
                print 'map_'+outpt+'_'+typ+'.fits.gz'
            data_cube, header = pyfits.getdata('map_'+outpt+'_'+typ+'.fits.gz', header=True)

            #get header parameters
            boxlen = header['boxlen']
            xlab, ylab = utils.get_labels(header)
            xmin, xmax = header['xmin'], header['xmax']
            ymin, ymax = header['ymin'], header['ymax']
            time = header['time'] #Myr
            if idx == 0 or len(type) > 1:
                unit = utils.get_unit(header)
                if unit == ' [H/cc]':
                    unit = ' [cm$^{-3}$]'
                if unit == ' [km/s]':
                    unit = ' [km.s$^{-1}$]'
                    
            #plot current image  
            im = axarr[ctr].imshow(data_cube, norm=my_norm[idx_typ], cmap=col_map[idx_typ], aspect='equal', \
                            extent=[xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen], origin='lower')

            #Show BH  
            if (show_bh):
                xsink, ysink, zsink = header['xsink'], header['ysink'], header['zsink']
                if abs(xsink-boxlen/2.) > 5 or abs(ysink-boxlen/2.) > 5 or abs(ysink-boxlen/2.) > 5: 
                    print 'BH abnormally far from center !!'
                    print 'xsink = {} kpc, ysink = {} kpc, zsink = {} kpc'.format(xsink, ysink, zsink)
                axarr[ctr].plot(xsink, ysink, 'w+', ms=20, mew=5)
                axarr[ctr].plot(xsink, ysink, 'b+', ms=15, mew=3)
                axarr[ctr].axis([xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen])


            #Add velocity field
            if (vel):
                direction = ['x','y','z']
                direction.remove(xlab)
                direction.remove(ylab)
                if (disk):
                    dk='_disk'
                elif (large):
                    dk='_large'
                elif (side):
                    dk='_side'
                else:
                    dk=''
                map1 = 'map_'+outpt+'_v_'+xlab+dk+'.fits.gz'
                map2 = 'map_'+outpt+'_v_'+ylab+dk+'.fits.gz'
                #if os.path.isfile('rdr_'+outpt+'_l16_v'+xlab+'_'+direction[0]+'.q') \
                #  and os.path.isfile('rdr_'+outpt+'_l16_v'+ylab+'_'+direction[0]+'.q'): #check existence of file first
                if os.path.isfile(map1) and os.path.isfile(map2):
                        if not silent:
                            print 'Velocity field :', map1, map2
                        vel_1, hdr1 = pyfits.getdata(map1, header=True)
                        vel_2, hdr2 = pyfits.getdata(map2, header=True)
                        #print vel_1.shape, vel_2.shape
                        #print 'unit = ', utils.get_unit(hdr1) #km/s
                
                        if boxlen != hdr1['boxlen'] or hdr1['boxlen'] != hdr2['boxlen']:
                            print 'Boxlen not equal !'
                            sys.exit(2)
                        if xmin != hdr1['xmin'] or hdr1['xmin'] != hdr2['xmin'] \
                        or xmax != hdr1['xmax'] or hdr1['xmax'] != hdr2['xmax'] \
                        or ymin != hdr1['ymin'] or hdr1['ymin'] != hdr2['ymin'] \
                        or ymax != hdr1['ymax'] or hdr1['ymax'] != hdr2['ymax'] :
                            print 'Problem with xmin/xmax/ymin/ymax !'
                            sys.exit(2)

                        npix = header['naxis1']
                        npix2 = header['naxis2']
                        if npix != hdr1['naxis1'] or npix2 != hdr1['naxis2'] or npix != hdr2['naxis1'] or npix2 != hdr2['naxis2']:
                        #if npix != hdr1['naxis1'] or npix2 != hdr2['naxis1'] or npix != hdr2['naxis2'] or npix2 != hdr2['naxis2']:
                            print 'Problem with number of pixels !'
                            print "npix, npix2, hdr1['naxis1'], hdr1['naxis2'], hdr2['naxis1'], hdr2['naxis2']"
                            print npix, npix2, hdr1['naxis1'], hdr1['naxis2'], hdr2['naxis1'], hdr2['naxis2']
                            sys.exit(2)

                        xl1, yl1 = utils.get_labels(hdr1)
                        xl2, yl2 = utils.get_labels(hdr2)
                        if xlab != xl1 or xlab != xl2 or ylab != yl1 or ylab != yl2:
                            print 'Problem with direction !!'
                            sys.exit(2)
            
                        x = np.arange(xmin,xmax,(xmax-xmin)*1./npix)*boxlen
                        y = np.arange(ymin,ymax,(ymax-ymin)*1./npix2)*boxlen

                        #skip elements, otherwise too many arrows (and too long to plot)
                        vel_1 = vel_1[::skip,::skip]*vel_factor
                        vel_2 = vel_2[::skip,::skip]*vel_factor
                        x = x[::skip]
                        y = y[::skip]

                        ###Norm of velocity (debug)
                        ###im = axarr[ctr].imshow(np.sqrt(vel_1**2+vel_2**2), norm=my_norm[idx_typ], cmap=col_map[idx_typ], aspect='equal', \
                        ###    extent=[xmin*boxlen,xmax*boxlen,ymin*boxlen,ymax*boxlen], origin='lower')
                        q = axarr[ctr].quiver(x,y,vel_1,vel_2, units='x',scale=npix)
                        #axarr[ctr].axhspan(96, 97, xmin=x_key/2., xmax=x_key*2., facecolor='white')
                        axarr[ctr].add_patch(Rectangle((x_key-0.16, y_key-0.1), rect_wdth, rect_ht, facecolor='white',transform=axarr[ctr].transAxes)) # (x, y), width, height
                        qk = axarr[ctr].quiverkey(q, x_key, y_key, key_length*vel_factor, r'$\mathdefault{%s%s^{%d}}$ km.s$^{-1}$' % ('', 10, np.log10(key_length)), \
                                                  fontproperties={'weight': 'roman', 'size': 'x-small'}, labelsep=0) #km s$^{-1}$

            if len(type) > 1:
                divider = make_axes_locatable(axarr[ctr])
                cax = divider.append_axes("right", size="10%", pad=0.05)
                cb = plt.colorbar(im, cax=cax)
                #if 'level' in typ:
                #    cb.set_ticks([9,10,11,12,13,14,15,16,17])
                if 'v_' in typ:
                    cb.set_ticks([-1e4,-1e3,-1e2,-1e1,-1e0,1e0,1e1,1e2,1e3,1e4]) ####Never delete this awsome code to write 10^x correclty
                    cb.ax.set_yticklabels([r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 4),r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 3),r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 2),r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 1),r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 0),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 0),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 1),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 2),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 3),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 4)])
                ctitle = header['outval'] + unit
                cb.set_label(ctitle)
    
                
            axarr[ctr].set_title('output_'+outpt+', t='+'{:.2f}'.format(time)+' Myr', size='medium')
            if (show_labels and show_bh): #'kpc' in header only in BH position. 
                plt.xlabel(xlab+' ['+(str(header['*xsink*']).split())[-2]+']')
                plt.ylabel(ylab+' ['+(str(header['*ysink*']).split())[-2]+']') #supposed to be 'kpc'
            ctr = ctr + 1

if len(type) == 1:
    cbaxes = fig.add_axes([0.1, 0.93, 0.8, 0.03]) 
    cb = plt.colorbar(im, cax = cbaxes, orientation='horizontal')
    if level in type[0]:
        cb.set_ticks([9,10,11,12,13,14,15,16,17])
    if 'v_' in type[0]:
        cb.set_ticks([-1e4,-1e3,-1e2,-1e1,-1e0,1e0,1e1,1e2,1e3,1e4])
        cb.ax.set_yticklabels([r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 4),r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 3),r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 2),r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 1),r'$\mathdefault{%s%s^{%d}}$' % ('-', 10, 0),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 0),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 1),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 2),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 3),r'$\mathdefault{%s%s^{%d}}$' % ('', 10, 4)])
        ctitle = header['outval'] + unit
        cb.set_label(ctitle)
        
plt.suptitle(utils.get_dir_name())
fig.set_tight_layout(True)
if not silent:
    print 'Loading plot...'
plt.show()
plt.savefig(figname+'.eps')

