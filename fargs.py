import sys

"""
FARGS is used to parse a simple command line and get the arguments as attributes of an object.

HOWTO:

	 1. Import the fargs module
import fargs
	
	 2. Define the parser, and some properties only used to generate the help.
argparser = fargs.Parser(name='Prog', description='Nice prog.', author='Florent Renaud', email='florent.renaud@cea.fr', version='1.0', date='25 February 2011')
	
	 3. Define the arguments (at least with their names)
argparser.add('inp', default='data.dat', help='Input file')
argparser.add('out', default='out.dat', help='Output file')
argparser.add('-theta', alias=['angle', 't'], default=0.0, help='Angle [deg]')
argparser.add('-vector', default='0,1,0', array=True, typ=float, help='Angle [deg]')
argparser.add('-a', default='foo', typ=str, help='Angle [deg]')
argparser.add('-b', help='Binary format')
	 	Each arguments can have alias(es), a default value, a type, and some help
	
		When the name does not begin with a dash, the argument is a value (typically the name of a file).
		Such arguments should be given in order in the command line.
		When the name begins with a dash (-), the argument must be set to a value. E.g. -theta=120
		
		Alias:	The alias can be used by the user in exactly the same way than the main name. E.g. -theta=1 is equivalent to -t=1
		Default:The default value is the one sent back to the code when the argument is not found. If not set, default is an empty string: ''
			If the default of a dashed argument is not defined, the argument is a boolean, set to False by default (i.e. a flag)
		Typ:	The type of the argument is usually those of the default value. Use typ to convert the argument into another type.
		Array:	When the value is an array-like, the user-supplied string is splitted wrt ',' and each value is converted into the type Typ.
			When typ is not set, array gives a list of strings.
			Note that the default value will be converted according to typ.
		Help:	The help is a short description of the argument that will be printed when option -h or --help is called.
		
		The special argument "_extra" groups all the non-dashed values into a list.
	
	 4. Run the parser
arg = argparser.parse()
		Set debug=True to see how the arguments are handled.
		The value of the argument 'name' is now accesible via: arg.name  ('name' has no dash in it)

EXEMPLE:
	with the exemple given above, the command lines below produce the corresponding arguments:
	
	./prog.py myfile -theta=120 -b		arg.inp == 'myfile'
						arg.out == 'out.dat'
						arg.theta == 120.0
						arg.vector = [0.0 , 1.0 , 0.0]
						arg.a == 'foo'
						arg.b == True

	./prog.py -a=12 myfile -t=3.2 myfile2	arg.inp == 'myfile'
						arg.out == 'myfile2'
						arg.theta == 3.2
						arg.vector = [0.0 , 1.0 , 0.0]
						arg.a == '12'
						arg.b == False

	./prog.py				arg.inp == 'data.dat'
						arg.out == 'out.dat'
						arg.theta == 0.0
						arg.vector = [0.0 , 1.0 , 0.0]
						arg.a == 'foo'
						arg.b == False

	./prog.py -vector=1,2,3			arg.inp == 'data.dat'
						arg.out == 'out.dat'
						arg.theta == 0.0
						arg.vector = [1.0 , 2.0 , 3.0]
						arg.a == 'foo'
						arg.b == False

Florent Renaud - 25 February 2011
"""

class UsableArgs():
	def __init__(self):
		pass

class Parser:
	def __init__(self, name=sys.argv[0], description='', author='', email='', version='1.0', date='1 January 2000'):
		self.val = [] # catalogue of non dashed arguments known by the code
		self.opt = [] # catalogue of dashed arguments known by the code
		self.arg = [] # arguments from the user
		self.out = UsableArgs() # init output object
		self.extra = False # Flag for the use of extra args
		self.info = {'name': name, 'description': description, 'author': author, 'email': email, 'version': version, 'date': date}


	def add(self, name, alias=None, default=False, typ=None, array=False, help=''):
		if name[0] == '-':
			name = name[1:]
			if alias is None:
				allnames = [name]
			else:
				allnames = [name]+[a for a in alias]

			if typ is None: typ = type(default) # if typ is not set, use the type of the default
			if array: default = self.convertarray(default, typ)
			self.opt.append({'name': allnames, 'default': default, 'help': help, 'typ': typ, 'array': array, 'set': False}) # update catalogue of dahsed args
		else:
			if name == '_extra':
				self.extra = True
				default = []
			if default is None: default = ''
			if typ is None: typ = type(default) # if typ is not set, use the type of the default
			if array: default = self.convertarray(default, typ)
			self.val.append({'name': name, 'default': default, 'help': help, 'typ': typ, 'array': array, 'set': False}) # update catalogue of non-dashed args

		setattr(self.out, name, default) # set attribute to temp default value


	def convertarray(self, string, typ):
		s = string.split(',')						
		if len(s) > 1:
			try: # try to convert the values
				out = [typ(item) for item in s]
			except ValueError: # if not possible, don't convert
				out = s
		else: # array is expected but only scalar found: convert scalar into one-element array
			try: # try to convert the value
				out = [typ(s[0])]
			except ValueError: # if not possible, don't convert
				out = [s[0]]
		return out
		
		
	def parse(self, arglist=sys.argv[1:], debug=False):
		if arglist == []: # no arguments passed: nothing to do
			return self.out
		
		if (arglist[0] == '-h') or (arglist[0] == '--help'):
			self.print_usage()
			sys.exit(2)

		val_i = 0
		for a in arglist: # for all the arguments in the command list
			found = False

			if a[0] == '-':	# if dashed
				l = a.split('=')
				for o in self.opt:
					if l[0][1:] in o['name']:
						if debug: print 'Option', o['name'][0], 'recognized'
						if o['set']:
							print '*** Error: option', o['name'][0], '(aliases =', o['name'], ') is set more than once.'
							print '*** Aborting ***'
							sys.exit(-1)
						if len(l) > 1:
							if o['array']: # deal with an array
								setattr(self.out, o['name'][0], self.convertarray(l[1], o['typ']))
							else:
								try: # try to convert the value
									setattr(self.out, o['name'][0], o['typ'](l[1]))
								except ValueError: # if not possible, don't convert
									setattr(self.out, o['name'][0], l[1])
						else: # boolean
							setattr(self.out, o['name'][0], True)
							
						o['set'] = True
						found = True
						break		    
			else: # if not dashed
				if self.extra:
					self.out._extra.append(a)
					val_i += 1
					found = True
				else:									
					if val_i < len(self.val):
						if debug: print 'Value', a, 'recognized as', self.val[val_i]['name']
#						if o['array']: # deal with an array
#							setattr(self.out, self.val[val_i]['name'], self.convertarray(self.val[val_i]['typ'],a))
#						else:
						try: #try to convert the value
							setattr(self.out, self.val[val_i]['name'], self.val[val_i]['typ'](a))
						except ValueError: # if not possible, don't convert
							setattr(self.out, self.val[val_i]['name'], a)
						val_i += 1
						found = True
				
			if not(found):
				print '*** Warning: argument', a, 'not recognized.'
	
		return self.out


	def print_usage(self):
		arg = ''
		opt = ''
		if len(self.val) ==0:
			arg += '\tNone'
		for a in self.val:
			if a != '_extra':
				arg += '\t%s\t\t\t%s\t(default: %s)\n' % (a['name'].upper(), a['help'], a['default'])
		for a in self.opt:
			al = ''
			if len(a['name']) > 1:
				al = '(alias: '
				for i in a['name'][1:]:
					al += '-'+i+', '
				al = al[:-2]+')'
			if a['typ'] == bool:
				opt += '\t-%s\t\t\t%s\t%s\n' % (a['name'][0], a['help'], al)
			else:
				opt += '\t-%s=[%s]\t\t%s\t%s\n' % (a['name'][0], a['default'], a['help'], al)
		opt += '\t-h\t\t\tThis help\t(alias: --help)'
		print ('NAME:\n\t%(name)s\n\nDESCRIPTION:\n\t%(description)s\n\nAUTHOR:\n\t%(author)s (%(email)s)\n\nARGUMENTS:\n'+arg+'\n\nOPTIONS:\n'+opt+'\n\nVERSION:\n\t%(version)s (%(date)s)') % self.info

